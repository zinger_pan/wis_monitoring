package gpr

import (
	"fmt"
	"github.com/go-openapi/strfmt"
	"net/http"
	"net/url"
)

// Catalog request
func (g *GPR) PerformSignalCreateRequest(expectedStatusCode int, body []byte, target interface{}) {
	g.PerformRequest("/signals/new", http.MethodPost, mimeTypeJSON, body, expectedStatusCode, target)
}

func (g *GPR) PerformSignalListRequest(expectedStatusCode int, urlValues url.Values, target interface{}) {
	addr := "/signals/list"
	if urlValues != nil {
		addr += "?" + urlValues.Encode()
	}
	g.PerformRequest(addr, http.MethodGet, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformSignalListPostRequest(expectedStatusCode int, urlValues url.Values, body []byte, target interface{}) {
	addr := "/signals/list"
	if urlValues != nil {
		addr += "?" + urlValues.Encode()
	}
	g.PerformRequest(addr, http.MethodPost, mimeTypeJSON, body, expectedStatusCode, target)
}

func (g *GPR) PerformSignalGetRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/signals/"+ID.String(), http.MethodGet, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformSignalArchiveRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/signals/archive?sgid="+ID.String(), http.MethodPost, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformSignalRemoveRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/signals/rem?sgid="+ID.String(), http.MethodPost, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformSignalRestoreRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/signals/restore?sgid="+ID.String(), http.MethodPost, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformSignalUpdateRequest(expectedStatusCode int, ID strfmt.UUID, body []byte, target interface{}) {
	g.PerformRequest(fmt.Sprintf("/signals/%s/update", ID), http.MethodPost, mimeTypeJSON, body, expectedStatusCode, target)
}
