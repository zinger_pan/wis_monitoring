package gpr

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"

	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"
)

const (
	authMethodBasic = "basic"
	authMethodToken = "token"
	authMethodNone  = "none"
)

const (
	DefaultBasicAuthLogin    = "login"
	DefaultBasicAuthPassword = "password"
)

// GPR abstracts ginkgo perform request
type GPR struct {
	authMethod        string
	logPerformRequest bool
	accessToken       string // for token auth
	login, password   string // for basic auth
	Logger            *logrus.Logger
	TestServer        **httptest.Server
}

// New returns a pointer to a new Ginkgo Perform Request (GPR) object with default settings
func New(ts *httptest.Server) *GPR {
	logrus.SetLevel(logrus.InfoLevel)
	return &GPR{
		authMethod:        authMethodNone,
		logPerformRequest: true,
		login:             DefaultBasicAuthLogin,
		password:          DefaultBasicAuthPassword,
		Logger:            logrus.StandardLogger(),
		TestServer:        &ts,
	}
}

// SetLogPerformRequest to value v
func (g *GPR) SetLogPerformRequest(v bool) { g.logPerformRequest = v }

// SetAuthMethodNone for requests
func (g *GPR) SetAuthMethodNone() { g.authMethod = authMethodNone }

// SetAuthMethodBasic for requests
func (g *GPR) SetAuthMethodBasic() { g.authMethod = authMethodBasic }

// SetAuthMethodToken for requests
func (g *GPR) SetAuthMethodToken() { g.authMethod = authMethodToken }

// SetAccessToken for AuthModeToken authorization mode
func (g *GPR) SetAccessToken(token string) { g.accessToken = token }

// AccessToken returns an access token set previously
func (g *GPR) AccessToken() string { return g.accessToken }

// SetLogin for AuthModeBasic authorization mode
func (g *GPR) SetLogin(login string) { g.login = login }

// Login returns a login set previously
func (g *GPR) Login() string { return g.login }

// SetPassword for AuthModeBasic authorization mode
func (g *GPR) SetPassword(password string) { g.password = password }

// Password returns a password set previously
func (g *GPR) Password() string { return g.password }

// doAuthorizedRequest with HTTP basic auth with given method, url and content for body
func (g *GPR) doAuthorizedRequest(method, url string, body io.Reader) (*http.Response, error) {
	request, err := http.NewRequest(method, url, body)
	ExpectWithOffset(3, err).NotTo(HaveOccurred(), "err from http.NewRequest in doAuthorizedRequest")
	request.Header.Add("content-type", "application/json")
	switch g.authMethod {
	case authMethodNone: // do nothing
	case authMethodToken:
		request.Header.Add("access-token", g.AccessToken())
	default: // default basic auth basic
		request.SetBasicAuth(g.Login(), g.Password())
	}
	return http.DefaultClient.Do(request)
}

// doRequest with given method, url and content for body
func (g *GPR) doRequest(method, url string, body io.Reader) (*http.Response, error) {
	request, err := http.NewRequest(method, url, body)
	ExpectWithOffset(3, err).NotTo(HaveOccurred(), "err from http.NewRequest in doRequest")
	request.Header.Add("content-type", "application/json")
	return http.DefaultClient.Do(request)
}

// HTTP server API request helpers

const (
	mimeTypeJSON = "application/json"
	mimeTypeXML  = "application/xml"

	apiPrefix = "/api/v1"
)

// PerformRequest is a helper func to perform HTTP API requests to the dbcore
func (g *GPR) PerformRequest(addr, method, mimeType string, JSON []byte, expectedStatusCode int, target interface{}) []byte {
	a := fmt.Sprintf("%s%s%s", (*g.TestServer).URL, apiPrefix, addr)

	var bytesReader io.Reader
	if JSON != nil {
		bytesReader = bytes.NewReader(JSON)
	}

	response, err := g.doAuthorizedRequest(method, a, bytesReader)
	ExpectWithOffset(2, err).NotTo(HaveOccurred(), "err from doAuthorizedRequest in PerformRequest")
	ExpectWithOffset(2, response).NotTo(BeNil(), "in PerformRequest")
	responseBody, err := io.ReadAll(response.Body)

	if g.logPerformRequest {
		g.Logger.Warnf(">>> PerformRequest() via %s on %s: <<< %s\n", method, addr, string(responseBody))
	}

	ExpectWithOffset(2, response.StatusCode).To(Equal(expectedStatusCode), "in PerformRequest")
	ExpectWithOffset(2, err).NotTo(HaveOccurred(), "err from io.ReadAll in PerformRequest")

	func() {
		if mimeType == "" {
			mimeType = mimeTypeJSON
		}
		switch mimeType {
		case mimeTypeJSON:
			ExpectWithOffset(2, json.Unmarshal(responseBody, &target)).To(Succeed())
		case mimeTypeXML:
			if reflect.ValueOf(target).Kind() != reflect.Ptr {
				// can't set non-ptr value
				return
			}
			indirect := reflect.Indirect(reflect.ValueOf(target))
			if response.StatusCode/100 == 2 {
				indirect.SetString(string(responseBody))
				return
			}
			if indirect.Kind() == reflect.Struct { // should be error structure
				ExpectWithOffset(2, json.Unmarshal(responseBody, &target)).To(Succeed())
			}
		}
	}()
	return responseBody
}

// MustIndentJSON indents the given JSON bytes b, panics on error
func (g *GPR) MustIndentJSON(b []byte) []byte {
	var indentedJSON bytes.Buffer
	if err := json.Indent(&indentedJSON, b, "", "  "); err != nil {
		panic(err)
	}
	return indentedJSON.Bytes()
}

// MustMarshalJSONToString v into a JSON string, panics on error
func (g *GPR) MustMarshalJSONToString(v interface{}) string {
	b, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return string(b)
}

// MustMarshalJSON is just like MustMarshalJSONToString but returns []byte
func (g *GPR) MustMarshalJSON(v interface{}) []byte { return []byte(g.MustMarshalJSONToString(v)) }
