package gpr

import (
	"bytes"
	"github.com/labstack/echo/v4"
	"github.com/mtfelian/utils"
	. "github.com/onsi/gomega"
	"io"
	"net/http"
)

// PerformStaticRequest makes an API call expecting some body in result
// method is a HTTP method to use. There it is http.MethodGet or http.MethodPost
// expectedStatus is an expected HTTP response status code.
// address is a full URL to call.
// body is an optional request body (or nil)
// Returns decoded body as bytes
func (g *GPR) PerformStaticRequest(method string, address string, body []byte, expectedStatus int) []byte {
	request, err := http.NewRequest(method, address, bytes.NewReader(body))
	ExpectWithOffset(1, err).NotTo(HaveOccurred())

	//if viper.GetString(config.Auth) == config.AuthBasic {
	//	request.SetBasicAuth(viper.GetString(config.Login), viper.GetString(config.Password))
	//}

	if body != nil && utils.SliceContains(method, []string{http.MethodPost, http.MethodPut}) {
		request.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	}

	resp, err := http.DefaultClient.Do(request)
	ExpectWithOffset(1, err).NotTo(HaveOccurred())
	ExpectWithOffset(1, resp.StatusCode).To(Equal(expectedStatus))

	b, err := io.ReadAll(resp.Body)
	ExpectWithOffset(1, err).NotTo(HaveOccurred())
	ExpectWithOffset(1, resp.Body.Close()).To(Succeed())
	return b
}

func (g *GPR) PerformSystemGetRequest(expectedStatusCode int, target interface{}) {
	g.PerformRequest("/system", http.MethodGet, mimeTypeJSON, nil, expectedStatusCode, target)
}
