package gpr

import (
	"fmt"
	"github.com/go-openapi/strfmt"
	"net/http"
	"net/url"
)

// Catalog request
func (g *GPR) PerformRuleCreateRequest(expectedStatusCode int, body []byte, target interface{}) {
	g.PerformRequest("/rules/new", http.MethodPost, mimeTypeJSON, body, expectedStatusCode, target)
}

func (g *GPR) PerformRuleListRequest(expectedStatusCode int, urlValues url.Values, target interface{}) {
	addr := "/rules/list"
	if urlValues != nil {
		addr += "?" + urlValues.Encode()
	}
	g.PerformRequest(addr, http.MethodGet, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformRuleListPostRequest(expectedStatusCode int, urlValues url.Values, body []byte, target interface{}) {
	addr := "/rules/list"
	if urlValues != nil {
		addr += "?" + urlValues.Encode()
	}
	g.PerformRequest(addr, http.MethodPost, mimeTypeJSON, body, expectedStatusCode, target)
}

func (g *GPR) PerformRuleGetRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/rules/"+ID.String(), http.MethodGet, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformRuleArchiveRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/rules/archive?rulid="+ID.String(), http.MethodPost, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformRuleRemoveRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/rules/rem?rulid="+ID.String(), http.MethodPost, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformRuleRestoreRequest(expectedStatusCode int, ID strfmt.UUID, target interface{}) {
	g.PerformRequest("/rules/restore?rulid="+ID.String(), http.MethodPost, mimeTypeJSON, nil, expectedStatusCode, target)
}

func (g *GPR) PerformRuleUpdateRequest(expectedStatusCode int, ID strfmt.UUID, body []byte, target interface{}) {
	g.PerformRequest(fmt.Sprintf("/rules/%s/update", ID), http.MethodPost, mimeTypeJSON, body, expectedStatusCode, target)
}
