package geonetwork

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"

	"monitoring-for-wis/config"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/response"
)

// GNClient implements all geonetwork client logic
type GNClient struct {
	httpClient *http.Client
	resolver   resolver.Resolver
}

// New returns a pointer to a new GNClient instance
func New(httpClient *http.Client, resolver resolver.Resolver) *GNClient {
	return &GNClient{httpClient: httpClient, resolver: resolver}
}

// GNGetRecordParams represents params for GNGetRecord method
type GNGetRecordParams struct{ ID string }

// setBasicAuth from incoming request to outgoing request or derive it from the configuration
func (c *GNClient) setBasicAuth(reqOut *http.Request, confAuth map[string]string) error {
	// if no basic auth were given at request, we try to derive it from configuration
	if confAuth == nil {
		return nil
	}

	// if have auth section in config
	token, ok := confAuth[resolver.ServiceGeoNetwork]
	if !ok {
		return nil
	}

	// and if have service-related subsection in config
	parts := strings.Split(token, ":")
	if len(parts) != 2 {
		return response.Default{Code: response.ErrFailedToParseBasicAuthInConfig, Message: fmt.Sprintf("parts: %v", parts)}
	}
	if parts[0]+parts[1] == "" { // both empty
		return nil
	}
	reqOut.SetBasicAuth(parts[0], parts[1])
	return nil
}

// GNGetRecord implements geonetwork get record client logic
func (c *GNClient) GNGetRecord(ctx context.Context, p GNGetRecordParams) ([]byte, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	gnService, err := c.resolver.Resolve(resolver.ServiceGeoNetwork)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToResolveService, Message: err.Error()}
	}

	addrURL := &url.URL{
		Host:   gnService.Host,
		Path:   path.Join(gnService.Path, "/srv/api/0.1/records", p.ID),
		Scheme: gnService.Scheme,
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, addrURL.String(), nil)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToConstructURL, Message: err.Error()}
	}

	conf := config.Clone()
	// prepare basic auth
	if err := c.setBasicAuth(req, conf.Auth); err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToRequest, Message: err.Error()}
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToReadBody, Message: err.Error()}
	}

	return respBody, nil
}
