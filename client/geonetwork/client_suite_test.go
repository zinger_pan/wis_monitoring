package geonetwork_test

import (
	"math/rand"
	"net/http"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	gnclient "monitoring-for-wis/client/geonetwork"
	"monitoring-for-wis/config"
	gnmock "monitoring-for-wis/mock/geonetwork"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/response"
)

const httpClientTimeout = 300 * time.Millisecond // increase this value in slow environment

var (
	gnMock   *gnmock.MockServer
	gnClient *gnclient.GNClient
)

var _ = Describe("register suite hooks", func() {
	BeforeSuite(func() {
		logrus.SetLevel(logrus.DebugLevel)
	})
	BeforeEach(func() {
		gnMock = gnmock.New()
		gnMock.SetBasicAuth("admin", "admin")

		viper.SetConfigType("yml")
		Expect(config.ViperLoadIntoCurrent()).To(Succeed())

		serviceResolver := resolver.NewConfigResolver(viper.GetViper())
		httpClient := &http.Client{
			Transport:     nil,
			CheckRedirect: nil,
			Jar:           nil,
			Timeout:       httpClientTimeout,
		}
		gnClient = gnclient.New(httpClient, serviceResolver)

		Expect(config.SoftSet(func(c *config.Entire) {
			c.Services = map[string]config.Service{
				resolver.ServiceGeoNetwork: {Host: gnMock.Server.URL, Path: "/geonetwork"},
			}
			c.Auth = map[string]string{
				resolver.ServiceGeoNetwork: "admin:admin",
			}
		})).To(Succeed())
	})

	It("checks that client works", func() {
		b, err := gnClient.GNGetRecord(nil, gnclient.GNGetRecordParams{ID: "urn:x-wmo:md:int.wmo.wis::SIKY20UAFF"})
		Expect(err).NotTo(HaveOccurred())
		Expect(string(b)).To(Equal(gnmock.APIRecordGetResponseStub))
	})

	It("checks that client fails on resolution error", func() {
		Expect(config.SoftSet(func(c *config.Entire) {
			c.Services = map[string]config.Service{
				"wrong service": {Host: gnMock.Server.URL, Path: "/geonetwork"},
			}
		})).To(Succeed())

		_, err := gnClient.GNGetRecord(nil, gnclient.GNGetRecordParams{ID: "urn:x-wmo:md:int.wmo.wis::SIKY20UAFF"})
		Expect(err).To(HaveOccurred())
		e, ok := err.(response.Default)
		Expect(ok).To(BeTrue())
		Expect(e.Code).To(Equal(response.ErrFailedToResolveService))
	})

	It("checks that client fails on HTTP client timeout", func() {
		gnMock.Hang()
		defer gnMock.Unhang()

		_, err := gnClient.GNGetRecord(nil, gnclient.GNGetRecordParams{ID: "urn:x-wmo:md:int.wmo.wis::SIKY20UAFF"})
		Expect(err).To(HaveOccurred())
		e, ok := err.(response.Default)
		Expect(ok).To(BeTrue())
		Expect(e.Code).To(Equal(response.ErrFailedToRequest))
	})
})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "GNClient Suite")
}
