package metadata

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"

	"monitoring-for-wis/config"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/response"
)

// MDTClient implements all modext client logic
type MDClient struct {
	httpClient *http.Client
	resolver   resolver.Resolver
}

// MDGetRecordParams represents params for GNGetRecord method
type MDGetRecordParams struct {
	ID        string            `json:"id,omitempty"`
	UrlParams map[string]string `json:"url_params,omitempty"`
}

type ItemResponse struct {
	Metadata struct {
		MdID     string `json:"mdid"`
		SrcID    string `json:"srcid"`
		Name     string `json:"name"`
		URN      string `json:"uuid"`
		Body     string `json:"body"`
		Created  int    `json:"created"`
		Updated  int    `json:"updated"`
		Archived int    `json:"archived"`
	} `json:"metadata"`
}

type ListItem struct {
	MdID     string `json:"mdid"`
	SrcID    string `json:"srcid"`
	URN      string `json:"uuid"`
	Name     string `json:"name"`
	Created  int    `json:"created"`
	Updated  int    `json:"updated"`
	Archived int    `json:"archived"`
}

type ListResponse struct {
	Info struct {
		Page      int `json:"page"`
		PageCount int `json:"pagecount"`
		PageSize  int `json:"pagesize"`
		Total     int `json:"total"`
	} `json:"info"`
	Items []ListItem `json:"items"`
}

// New returns a pointer to a new MDClient instance
func New(httpClient *http.Client, resolver resolver.Resolver) *MDClient {
	return &MDClient{httpClient: httpClient, resolver: resolver}
}

// setBasicAuth from incoming request to outgoing request or derive it from the configuration
func (c *MDClient) setBasicAuth(reqOut *http.Request, confAuth map[string]string) error {
	// if no basic auth were given at request, we try to derive it from configuration
	if confAuth == nil {
		return nil
	}

	// if have auth section in config
	token, ok := confAuth[resolver.ServiceMetadata]
	if !ok {
		return nil
	}

	// and if have service-related subsection in config
	parts := strings.Split(token, ":")
	if len(parts) != 2 {
		return response.Default{Code: response.ErrFailedToParseBasicAuthInConfig, Message: fmt.Sprintf("parts: %v", parts)}
	}
	if parts[0]+parts[1] == "" { // both empty
		return nil
	}
	reqOut.SetBasicAuth(parts[0], parts[1])
	return nil
}

func (c *MDClient) MDGetItem(ctx context.Context, id string) ([]byte, error) {
	return c.MDGetRecord(ctx, "body", MDGetRecordParams{ID: id})
}

// MDGetList implements metadata get record list client logic
func (c *MDClient) MDGetList(ctx context.Context, page int) ([]byte, error) {
	params := map[string]string{
		"page": strconv.Itoa(page),
	}

	return c.MDGetRecord(ctx, "list", MDGetRecordParams{UrlParams: params})
}

// MDGetRecord implements metadata get record client logic
func (c *MDClient) MDGetRecord(ctx context.Context, pathUrl string, params MDGetRecordParams) ([]byte, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	mdtService, err := c.resolver.Resolve(resolver.ServiceMetadata)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToResolveService, Message: err.Error()}
	}

	addrURL := &url.URL{
		Host:   mdtService.Host,
		Path:   path.Join(mdtService.Path, pathUrl, params.ID),
		Scheme: mdtService.Scheme,
	}

	requestBody, err := json.Marshal(params)

	if len(params.UrlParams) > 0 {
		q, _ := url.ParseQuery(addrURL.RawQuery)
		for key, val := range params.UrlParams {
			q.Add(key, val)
		}
		addrURL.RawQuery = q.Encode()
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, addrURL.String(), bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToConstructURL, Message: err.Error()}
	}

	req.Header.Set("Content-Type", "application/json")

	conf := config.Clone()
	// prepare basic auth
	if err := c.setBasicAuth(req, conf.Auth); err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToRequest, Message: err.Error()}
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToReadBody, Message: err.Error()}
	}

	return respBody, nil
}
