package dbcore

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"monitoring-for-wis/keeper/models"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"

	"github.com/go-openapi/strfmt"
	"monitoring-for-wis/config"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/response"
)

var (
	ErrParseHeaderStr     = "couldn't parse header for jrid %s"
	ErrParseMessageFMTStr = "couldn't parse fmt for jrid %s"
)

// DCClient implements all client logic
type DCClient struct {
	httpClient *http.Client
	resolver   resolver.Resolver
	//PostCatalogStates(*dbcore.CatalogStatesBody) error
}

// New returns a pointer to a new DCClient instance
func New(httpClient *http.Client, resolver resolver.Resolver) *DCClient {
	return &DCClient{httpClient: httpClient, resolver: resolver}
}

// setBasicAuth from incoming request to outgoing request or derive it from the configuration
func (c *DCClient) setBasicAuth(reqOut *http.Request, confAuth map[string]string) error {
	// if no basic auth were given at request, we try to derive it from configuration
	if confAuth == nil {
		return nil
	}

	// if have auth section in config
	token, ok := confAuth[resolver.ServiceDbCore]
	if !ok {
		return nil
	}

	// and if have service-related subsection in config
	parts := strings.Split(token, ":")
	if len(parts) != 2 {
		return response.Default{Code: response.ErrFailedToParseBasicAuthInConfig, Message: fmt.Sprintf("parts: %v", parts)}
	}
	if parts[0]+parts[1] == "" { // both empty
		return nil
	}
	reqOut.SetBasicAuth(parts[0], parts[1])
	return nil
}

// DCSystem implements DbCore system info request API client logic
func (c *DCClient) DCSystem(ctx context.Context) ([]byte, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	dcService, err := c.resolver.Resolve(resolver.ServiceDbCore)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToResolveService, Message: err.Error()}
	}

	addrURL := &url.URL{
		Host:   dcService.Host,
		Scheme: dcService.Scheme,
		Path:   path.Join(dcService.Path, "system"),
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, addrURL.String(), nil)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToConstructURL, Message: err.Error()}
	}

	conf := config.Clone()
	// prepare basic auth
	if err := c.setBasicAuth(req, conf.Auth); err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToRequest, Message: err.Error()}
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToReadBody, Message: err.Error()}
	}

	return respBody, nil
}

// ListJournalRecordResponse represents a response for ListJournalRecords method
type ListJournalRecordResponse struct {
	Records struct {
		Count uint                   `json:"count"`
		Items []models.JournalRecord `json:"items"`
	} `json:"jrecs"`
}

func FindJournalRecordByKey(records []models.JournalRecord, key string) *models.JournalRecord {
	for _, jr := range records {
		if key == jr.Key {
			return &jr
		}
	}
	return nil
}

func daysIn(m time.Month, year int) int {
	// This is equivalent to time.daysIn(m, year).
	return time.Date(year, m+1, 0, 0, 0, 0, 0, time.UTC).Day()
}

func (c *DCClient) unmarshalResponseBody(body io.Reader, target interface{}) error {
	var base struct {
		Meta struct {
			Error string `json:"error"`
		} `json:"meta"`
		Response json.RawMessage `json:"response"`
	}

	b, err := io.ReadAll(body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(b, &base); err != nil {
		return err
	}

	if base.Meta.Error != "" {
		return fmt.Errorf("%s", base.Meta.Error)
	}

	return json.Unmarshal(base.Response, &target)
}

// ListJournalRecordsParams represents params for ListJournalRecords method
type ListJournalRecordsParams struct {
	From    strfmt.UUID
	StartAt time.Time
	URNs    []string
	Count   int
}

// ListJournalRecords from the DbCore
func (c *DCClient) ListJournalRecords(ctx context.Context, p ListJournalRecordsParams) ([]models.JournalRecord, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	dcService, err := c.resolver.Resolve(resolver.ServiceDbCore)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToResolveService, Message: err.Error()}
	}

	addrURL := &url.URL{
		Host:   dcService.Host,
		Scheme: dcService.Scheme,
		Path:   path.Join(dcService.Path, "storage/list"),
		RawQuery: func() string {
			queryParams := make(url.Values)
			if p.From != "" {
				queryParams.Set("from", p.From.String())
			}
			if !p.StartAt.IsZero() {
				queryParams.Set("startAt", fmt.Sprintf("%d", p.StartAt.Unix()))
			}
			if p.Count > 0 {
				queryParams.Set("count", fmt.Sprintf("%d", p.Count))
			}
			if len(p.URNs) > 0 {
				queryParams.Set("urn", strings.Join(p.URNs, ","))
			}
			queryParams.Set("flag", "forward")
			return queryParams.Encode()
		}(),
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, addrURL.String(), nil)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToConstructURL, Message: err.Error()}
	}

	conf := config.Clone()
	// prepare basic auth
	if err := c.setBasicAuth(req, conf.Auth); err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, response.Default{Code: response.ErrFailedToRequest, Message: err.Error()}
	}
	defer resp.Body.Close()

	var r ListJournalRecordResponse
	if err = c.unmarshalResponseBody(resp.Body, &r); err != nil {
		return nil, response.Default{Code: response.ErrFailedToUnmarshalResponse, Message: err.Error()}
	}

	return r.Records.Items, nil
}
