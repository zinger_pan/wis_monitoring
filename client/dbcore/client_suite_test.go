package dbcore_test

import (
	"math/rand"
	"net/http"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	dcclient "monitoring-for-wis/client/dbcore"
	"monitoring-for-wis/config"
	dcmock "monitoring-for-wis/mock/dbcore"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/response"
)

const httpClientTimeout = 300 * time.Millisecond // increase this value in slow environment

var (
	dcMock   *dcmock.MockServer
	dcClient *dcclient.DCClient
)

var _ = Describe("register suite hooks", func() {
	BeforeSuite(func() {
		logrus.SetLevel(logrus.DebugLevel)
	})
	BeforeEach(func() {
		dcMock = dcmock.New()
		dcMock.SetBasicAuth("admin", "admin")

		viper.SetConfigType("yml")
		Expect(config.ViperLoadIntoCurrent()).To(Succeed())

		serviceResolver := resolver.NewConfigResolver(viper.GetViper())
		httpClient := &http.Client{
			Transport:     nil,
			CheckRedirect: nil,
			Jar:           nil,
			Timeout:       httpClientTimeout,
		}
		dcClient = dcclient.New(httpClient, serviceResolver)

		Expect(config.SoftSet(func(c *config.Entire) {
			c.Services = map[string]config.Service{
				resolver.ServiceDbCore: {Host: dcMock.Server.URL, Path: "/api/v2"},
			}
			c.Auth = map[string]string{
				resolver.ServiceDbCore: "admin:admin",
			}
		})).To(Succeed())
	})

	It("checks that client works", func() {
		b, err := dcClient.DCSystem(nil)
		Expect(err).NotTo(HaveOccurred())
		Expect(b).To(MatchJSON(dcmock.APISystemResponseStub))
	})

	It("checks that client fails on resolution error", func() {
		Expect(config.SoftSet(func(c *config.Entire) {
			c.Services = map[string]config.Service{
				"wrong service": {Host: dcMock.Server.URL, Path: "/api/v2"},
			}
		})).To(Succeed())

		_, err := dcClient.DCSystem(nil)
		Expect(err).To(HaveOccurred())
		e, ok := err.(response.Default)
		Expect(ok).To(BeTrue())
		Expect(e.Code).To(Equal(response.ErrFailedToResolveService))
	})

	It("checks that client fails on HTTP client timeout", func() {
		dcMock.Hang()
		defer dcMock.Unhang()

		_, err := dcClient.DCSystem(nil)
		Expect(err).To(HaveOccurred())
		e, ok := err.(response.Default)
		Expect(ok).To(BeTrue())
		Expect(e.Code).To(Equal(response.ErrFailedToRequest))
	})
})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "DCClient Suite")
}
