package signal

import (
	"net/http"

	"monitoring-for-wis/keeper/models"

	"gorm.io/datatypes"

	"github.com/AlekSi/pointer"
	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// GetParams represents parameters for Get handler
type GetParams struct {
	ID strfmt.UUID `param:"id" validate:"required,uuid"`
}

// GetResponse represents response for Get handler
type GetResponse struct {
	Signal getResponseSignal `json:"signal"`
}

// getResponseSignal represents an Items field in GetResponse
type getResponseSignal struct {
	ID        strfmt.UUID    `json:"sgid"`
	CompanyID *strfmt.UUID   `json:"compid"`
	Kind      string         `json:"kind"`
	Caption   string         `json:"caption"`
	Msg       string         `json:"msg"`
	Comment   string         `json:"comm"`
	Data      datatypes.JSON `json:"data"`
	Created   int64          `json:"created"`
	Updated   int64          `json:"updated"`
	Archived  *int64         `json:"archived"`
}

// newGetResponse converts models to GetResponse
func newGetResponse(signal models.Signal) GetResponse {
	r := GetResponse{Signal: getResponseSignal{
		ID:        signal.ID,
		Kind:      signal.Kind,
		CompanyID: signal.CompanyID.ToUUIDPtr(),
		Caption:   signal.Caption,
		Msg:       signal.Msg,
		Comment:   signal.Comment,
		Data:      signal.Data,
		Created:   signal.CreatedAt.Unix(),
		Updated:   signal.UpdatedAt.Unix(),
	}}
	if signal.Archived != nil {
		r.Signal.Archived = pointer.ToInt64(signal.Archived.Unix())
	}

	return r
}

// Get is an API handler to get one company
func Get(c echo.Context) error {
	var code int
	p := new(GetParams)
	if err := c.Bind(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	signal, err := s.Monitoring.GetSignal(c.Request().Context(), p.ID)
	if err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}
	if signal == nil {
		code = http.StatusNotFound
		return c.JSON(code, response.Default{Code: response.ErrContentNotFound, Message: "signal not found"})
	}

	return c.JSON(http.StatusOK, newGetResponse(*signal))
}
