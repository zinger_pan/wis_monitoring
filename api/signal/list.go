package signal

import (
	"math"
	"net/http"
	"time"

	"github.com/AlekSi/pointer"

	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"github.com/mtfelian/utils"
	"monitoring-for-wis/keeper/models"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// ListParams represents parameters for ListRules handler
type ListParams struct {
	Page     int      `json:"page" validate:"omitempty,gt=0"`
	PageSize int      `json:"pageSize" validate:"omitempty,gt=0"`
	Order    []string `query:"order" validate:"omitempty,oneof=name created updated"`
	OrderDir string   `query:"orderDir" validate:"omitempty,oneof=asc desc"`

	Kinds       []string      `query:"kinds" json:"kinds" validate:"gte=0,lte=100,dive,gt=0,lte=255"`
	IDs         []strfmt.UUID `query:"sgid" json:"sgid" validate:"gte=0,lte=100,dive,uuid"`
	CompanyIDs  []strfmt.UUID `query:"compid" validate:"gte=0,lte=100,dive,uuid"`
	UpdatedFrom int64         `query:"updatedFrom" validate:"omitempty,gt=0"`
	UpdatedTo   int64         `query:"updatedTo" validate:"omitempty,gt=0"`
	SearchStr   string        `query:"searchStr" validate:"omitempty,gt=0"`
	Search      []string      `query:"search" validate:"omitempty,gt=0,dive,oneof=caption kind"`
	Flags       []string      `query:"flags" validate:"gte=0,lte=100,dive,gt=0,oneof=all archiveonly"`
}

const defaultPageSize = 100

// normalizePagination given
func (p *ListParams) normalizePagination() {
	if p.Page < 1 {
		p.Page = 1
	}
	if p.PageSize < 1 {
		p.PageSize = defaultPageSize
	}
	if len(p.OrderDir) == 0 {
		p.OrderDir = "desc"
	}
	for i := range p.Order {
		p.Order[i] += " " + p.OrderDir
	}
}

// ToModel converts ListParams to model
func (p ListParams) toModel() models.ListSignalParams {
	r := models.ListSignalParams{
		Page:            p.Page,
		PageSize:        p.PageSize,
		IDs:             p.IDs,
		CompanyIDs:      p.CompanyIDs,
		Order:           p.Order,
		OrderDir:        p.OrderDir,
		Kinds:           p.Kinds,
		SearchStr:       p.SearchStr,
		Search:          p.Search,
		FlagArchiveOnly: utils.SliceContains("archiveonly", p.Flags),
		FlagAll:         utils.SliceContains("all", p.Flags),
	}
	if p.UpdatedFrom != 0 {
		r.UpdatedFrom = time.Unix(p.UpdatedFrom, 0)
	}

	if p.UpdatedTo != 0 {
		r.UpdatedTo = time.Unix(p.UpdatedTo, 0)
	}
	return r
}

// ListResponseInfo represents Info field in ListResponse
type ListResponseInfo struct {
	Page      int `json:"page"`
	PageSize  int `json:"pagesize"`
	PageCount int `json:"pagecount"`
	Total     int `json:"total"`
}

// ListResponse represents response for List handler
type ListResponse struct {
	Info  ListResponseInfo  `json:"info"`
	Items ListResponseItems `json:"items"`
}

// listResponseItem represents an Item of Items field in ListResponse
type listResponseItem struct {
	ID        strfmt.UUID  `json:"sgid"`
	CompanyID *strfmt.UUID `json:"compid"`
	Kind      string       `json:"kind"`
	Caption   string       `json:"caption"`
	Msg       string       `json:"msg"`
	Comment   string       `json:"comm"`
	Created   int64        `json:"created"`
	Updated   int64        `json:"updated"`
	Archived  *int64       `json:"archived"`
}

// newListResponse converts models to ListResponse
func newListResponse(page, pageSize, total int, signals []models.Signal) ListResponse {
	return ListResponse{
		Info: ListResponseInfo{
			Page:      page,
			PageSize:  pageSize,
			PageCount: int(math.Ceil(float64(total) / float64(pageSize))),
			Total:     total,
		},
		Items: func() []listResponseItem {
			res := make([]listResponseItem, len(signals))
			for i, item := range signals {
				res[i] = listResponseItem{
					ID:        item.ID,
					CompanyID: item.CompanyID.ToUUIDPtr(),
					Kind:      item.Kind,
					Caption:   item.Caption,
					Msg:       item.Msg,
					Comment:   item.Comment,
					Created:   item.CreatedAt.Unix(),
					Updated:   item.UpdatedAt.Unix(),
				}
				if item.Archived != nil {
					res[i].Archived = pointer.ToInt64(item.Archived.Unix())
				}
			}
			return res
		}(),
	}
}

// ListResponseItems is a slice of listResponseItem
type ListResponseItems []listResponseItem

// IDs returns a slice of all IDs
func (o ListResponseItems) IDs() []strfmt.UUID {
	res := make([]strfmt.UUID, len(o))
	for i, content := range o {
		res[i] = content.ID
	}
	return res
}

// List is an API handler to list signals
func List(c echo.Context) error {
	var code int
	p := new(ListParams)
	if err := (&echo.DefaultBinder{}).BindQueryParams(c, p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := (&echo.DefaultBinder{}).BindBody(c, p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	p.normalizePagination()

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()

	signals, total, err := s.Monitoring.ListSignals(c.Request().Context(), p.toModel())
	if err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusOK, newListResponse(p.Page, p.PageSize, total, signals))
}
