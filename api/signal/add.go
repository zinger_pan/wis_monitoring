package signal

import (
	"net/http"

	"github.com/mtfelian/utils"

	"gorm.io/datatypes"

	"github.com/go-openapi/strfmt"

	"github.com/labstack/echo/v4"
	uuid "github.com/satori/go.uuid"
	"monitoring-for-wis/keeper/models"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// AddParams represents parameters for Add handler
type AddParams struct {
	ID        strfmt.UUID    `json:"sgid" validate:"omitempty,uuid"`
	CompanyID strfmt.UUID    `json:"compid" validate:"omitempty,omitempty,uuid"`
	Kind      string         `json:"kind" validate:"required,gte=0,gt=0,lte=255"`
	Caption   string         `json:"caption" validate:"required,gte=0,gt=0,lte=255"`
	Msg       string         `json:"msg" validate:"required,gte=0,gt=0,lte=255"`
	Data      datatypes.JSON `json:"data" validate:"omitempty,be_json_object"`
}

// ToModel converts AddParams to model
func (p AddParams) ToModel() (res models.Signal) {
	res = models.Signal{
		ID:        p.ID,
		Kind:      p.Kind,
		Caption:   p.Caption,
		Msg:       p.Msg,
		CompanyID: models.NewNullUUIDFromUUID(p.CompanyID),
		Data:      utils.MushMarshalJSON(p.Data),
	}
	if len(res.ID) == 0 {
		res.ID = strfmt.UUID(uuid.NewV4().String())
	}
	return
}

// AddResponse represents response for AddRule handler
type AddResponse struct {
	ID strfmt.UUID `json:"sgid"`
}

func newAddResponse(ID strfmt.UUID) AddResponse {
	return AddResponse{
		ID: ID,
	}
}

// Add is an API handler to add rules
func Add(c echo.Context) error {
	var code int
	p := new(AddParams)
	if err := c.Bind(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	id, err := s.Monitoring.AddSignal(c.Request().Context(), p.ToModel())
	if err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusCreated, newAddResponse(id))
}
