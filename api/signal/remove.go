package signal

import (
	"net/http"

	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// RemoveParams represents parameters for Remove handler
type RemoveParams struct {
	ID strfmt.UUID `query:"sgid" validate:"required,uuid"`
}

// RemoveResponse represents response for RemoveCompany handler
type RemoveResponse struct{}

// Remove is an API handler to remove rules
func Remove(c echo.Context) error {
	var code int
	p := new(RemoveParams)
	if err := (&echo.DefaultBinder{}).BindQueryParams(c, p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	if err := s.Monitoring.RemoveSignal(c.Request().Context(), p.ID); err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusOK, RemoveResponse{})
}
