package api

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// DCSystemParams represents parameters for DCSystem handler
type DCSystemParams struct {
}

// DCSystemResponse represents response for DCSystem handler
type DCSystemResponse struct {
}

// DCSystem is an API handler to proxy DC system request
func DCSystem(c echo.Context) error {
	var code int
	p := new(DCSystemParams)
	if err := c.Bind(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	body, err := s.DCClient.DCSystem(c.Request().Context())
	if err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSONBlob(http.StatusOK, body)
}
