package rule

import (
	"net/http"

	"monitoring-for-wis/keeper/models"

	"gorm.io/datatypes"

	"github.com/AlekSi/pointer"
	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// GetParams represents parameters for Get handler
type GetParams struct {
	ID strfmt.UUID `param:"id" validate:"required,uuid"`
}

// GetResponse represents response for Get handler
type GetResponse struct {
	Rule getResponseCompany `json:"rule"`
}

// getResponseCompany represents an Items field in GetResponse
type getResponseCompany struct {
	ID        strfmt.UUID    `json:"rulid"`
	CompanyID *strfmt.UUID   `json:"compid"`
	Name      string         `json:"name"`
	URN       string         `json:"urn"`
	Data      datatypes.JSON `json:"data"`
	Created   int64          `json:"created"`
	Updated   int64          `json:"updated"`
	Archived  *int64         `json:"archived"`
}

// newGetResponse converts models to GetResponse
func newGetResponse(rule models.Rule) GetResponse {
	r := GetResponse{Rule: getResponseCompany{
		ID:        rule.ID,
		Name:      rule.Name,
		CompanyID: rule.CompanyID.ToUUIDPtr(),
		URN:       rule.URN,
		Data:      rule.ToObjectData(),
		Created:   rule.CreatedAt.Unix(),
		Updated:   rule.UpdatedAt.Unix(),
	}}
	if rule.Archived != nil {
		r.Rule.Archived = pointer.ToInt64(rule.Archived.Unix())
	}

	return r
}

// Get is an API handler to get one rule
func Get(c echo.Context) error {
	var code int
	p := new(GetParams)
	if err := c.Bind(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	rule, err := s.Monitoring.GetRuleByID(c.Request().Context(), p.ID)
	if err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}
	if rule == nil {
		code = http.StatusNotFound
		return c.JSON(code, response.Default{Code: response.ErrContentNotFound, Message: "rule not found"})
	}

	return c.JSON(http.StatusOK, newGetResponse(*rule))
}
