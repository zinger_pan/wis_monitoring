package rule

import (
	"net/http"

	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// ArchiveParams represents parameters for Archive handler
type ArchiveParams struct {
	ID strfmt.UUID `query:"rulid" validate:"required,uuid"`
}

// ArchiveResponse represents response for Archive handler
type ArchiveResponse struct{}

// Archive is an API handler to archive rules
func Archive(c echo.Context) error {
	var code int
	p := new(ArchiveParams)

	if err := (&echo.DefaultBinder{}).BindQueryParams(c, p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	if err := s.Monitoring.ArchiveRule(c.Request().Context(), p.ID); err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusOK, ArchiveResponse{})
}
