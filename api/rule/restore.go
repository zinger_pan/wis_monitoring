package rule

import (
	"net/http"

	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// RestoreParams represents parameters for Restore handler
type RestoreParams struct {
	ID strfmt.UUID `query:"rulid" validate:"required,uuid"`
}

// RestoreResponse represents response for Restore handler
type RestoreResponse struct{}

// Restore is an API handler to restore rules
func Restore(c echo.Context) error {
	var code int
	p := new(RestoreParams)
	if err := (&echo.DefaultBinder{}).BindQueryParams(c, p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	if err := s.Monitoring.RestoreRule(c.Request().Context(), p.ID); err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusOK, RestoreResponse{})
}
