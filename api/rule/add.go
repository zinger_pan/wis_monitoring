package rule

import (
	"net/http"

	"github.com/mtfelian/utils"

	"gorm.io/datatypes"

	"github.com/go-openapi/strfmt"

	"github.com/labstack/echo/v4"
	uuid "github.com/satori/go.uuid"
	"monitoring-for-wis/keeper/models"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// AddParams represents parameters for Add handler
type AddParams struct {
	ID        strfmt.UUID    `json:"rulid" validate:"omitempty,uuid"`
	CompanyID strfmt.UUID    `json:"compid" validate:"required,omitempty,uuid"`
	URN       string         `json:"urn" validate:"required,gte=0,lte=100,gt=0,lte=255"`
	Name      string         `json:"name" validate:"gt=0,lte=200"`
	Data      datatypes.JSON `json:"data" validate:"omitempty,be_json_object"`
}

// ToModel converts AddParams to model
func (p AddParams) ToModel() (res models.Rule) {
	ruleData := new(models.RuleData)
	utils.MustMarshalUnmarshalJSON(p.Data, ruleData)
	name := p.Name
	if p.Name == "" {
		name = p.URN
	}
	res = models.Rule{
		ID:        p.ID,
		Name:      name,
		URN:       p.URN,
		CompanyID: models.NewNullUUIDFromUUID(p.CompanyID),
		Schedule:  ruleData.Schedule,
		RegExp:    ruleData.RegExp,
	}
	if len(res.ID) == 0 {
		res.ID = strfmt.UUID(uuid.NewV4().String())
	}
	return
}

// AddResponse represents response for AddRule handler
type AddResponse struct {
	ID strfmt.UUID `json:"rulid"`
}

// Add is an API handler to add rules
func Add(c echo.Context) error {
	var code int
	p := new(AddParams)
	if err := c.Bind(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	if _, err := s.Monitoring.AddRules(c.Request().Context(), []models.Rule{p.ToModel()}); err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusCreated, AddResponse{})
}
