package rule

import (
	"net/http"

	"github.com/mtfelian/utils"

	"monitoring-for-wis/keeper/models"

	"github.com/go-openapi/strfmt"
	"github.com/labstack/echo/v4"
	"gorm.io/datatypes"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

// UpdateParams represents parameters for Update handler
type UpdateParams struct {
	ID   strfmt.UUID    `param:"id" validate:"required,uuid"`
	Name *string        `json:"name" validate:"omitempty,gte=0,lte=200"`
	Data datatypes.JSON `json:"data" validate:"omitempty,be_json_object"`
}

// ToFields converts UpdateParams to fields map
func (p UpdateParams) ToFields() (res map[string]interface{}) {
	res = make(map[string]interface{})
	if p.Name != nil {
		res[models.ObjectFieldName] = *p.Name
	}

	if p.Data != nil {
		res[models.ObjectFieldData] = datatypes.JSON(utils.MushMarshalJSON(p.Data))
	}
	return
}

// UpdateResponse represents response for Update handler
type UpdateResponse struct{}

// Update is an API handler to update rules
func Update(c echo.Context) error {
	var code int
	p := new(UpdateParams)
	if err := c.Bind(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrBindingRequest, Message: err.Error()})
	}

	if err := c.Validate(p); err != nil {
		code = http.StatusUnprocessableEntity
		return c.JSON(code, response.Default{Code: response.ErrValidationRequest, Message: err.Error()})
	}

	s := service.Get()
	if err := s.Monitoring.UpdateRule(c.Request().Context(), p.ID, p.ToFields()); err != nil {
		code = http.StatusInternalServerError
		return c.JSON(code, err)
	}

	return c.JSON(http.StatusOK, UpdateResponse{})
}
