package config

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/mtfelian/deepcopy"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

var (
	mu        sync.RWMutex
	callbacks sync.Map

	errValuesShouldBeOfOneType = errors.New("values should be of one type")
)

// FileName for configuration file
const DevFileName = "config.dev.yml"
const FileName = "config.yml"

// executable flag names
const (
	Port     = "port"
	LogLevel = "loglevel"
)

// ErrorSpecifyPort is raised when port flag was not specified
var ErrorSpecifyPort = errors.New("should specify --port command line flag")

func checkRequired() error {
	if viper.GetInt(Port) == 0 {
		return ErrorSpecifyPort
	}
	return nil
}

// Parse the configuration data from different sources
func Parse() (*viper.Viper, error) {
	current = &Entire{}
	if err := parseFlags(); err != nil {
		log.Fatalln("Error parsing flags:", err)
	}
	if err := parseConfigFile(); err != nil {
		log.Fatalln("Error parsing config file:", err)
	}
	if err := checkRequired(); err != nil {
		log.Fatalln(err)
	}
	return viper.GetViper(), nil
}

// parseFlags from command line
func parseFlags() error {
	pflag.UintVar(&current.Port, Port, 0, "application HTTP port")

	pflag.StringVar(&current.LogLevel, LogLevel, logrus.InfoLevel.String(), "logging level")

	pflag.Parse()
	return viper.BindPFlags(pflag.CommandLine)
}

// parseConfigFile
func parseConfigFile(paths ...string) error {
	viper.AddConfigPath(".")
	for _, path := range paths {
		viper.AddConfigPath(path)
	}
	//viper.SetConfigName(viper.GetString(DevFileName))
	viper.SetConfigName(DevFileName)
	viper.SetConfigType("yml")

	viper.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	if err := viper.ReadInConfig(); err != nil {
		viper.SetConfigName(FileName)
		viper.SetConfigType("yml")

		if err := viper.ReadInConfig(); err != nil {
			if _, ok := err.(viper.ConfigFileNotFoundError); ok {
				return nil
			}
			return fmt.Errorf("failed to load config file: %v", err)
		}
	}

	setDefaults()
	return ViperLoadIntoCurrent()
}

// ViperLoadIntoCurrent unmarshals viper configuration on current configuration pointer
func ViperLoadIntoCurrent() error {
	var newConfig Entire
	if err := viper.Unmarshal(&newConfig); err != nil {
		return err
	}
	return Set(newConfig, true, false)
}

// Entire describes command line options
type Entire struct {
	Port             uint               `mapstructure:"port" json:"port" yaml:"port"`
	LogLevel         string             `mapstructure:"logLevel" json:"logLevel" yaml:"logLevel"`
	Services         map[string]Service `mapstructure:"services" json:"services" yaml:"services"`
	HTTPClient       HTTPClient         `mapstructure:"httpClient" json:"httpClient" yaml:"httpClient"`
	DefaultBatchSize int                `mapstructure:"defaultBatchSize" json:"defaultBatchSize" yaml:"defaultBatchSize"`
	Storage          Storage            `mapstructure:"storage" json:"storage" yaml:"storage"`
	Auth             map[string]string  `mapstructure:"auth" json:"auth" yaml:"auth"`
	Tickers          Tickers            `mapstructure:"tickers" json:"tickers" yaml:"tickers"`
	MTOffsets        MTOffsets          `mapstructure:"mtOffsets" json:"mtOffsets" yaml:"mtOffsets"`
}

// Clone makes a copy of it
func (c Entire) Clone() Entire { return deepcopy.Copy(c).(Entire) }

// current is an application command line parameters
var current *Entire

// Get returns a pointer to the entire configuration
// Don't use it to set fields to avoid race condition
func Get() *Entire {
	mu.RLock()
	defer mu.RUnlock()
	return current
}

// Clone the entire current configuration and return a copy.
func Clone() Entire {
	mu.RLock()
	defer mu.RUnlock()
	return current.Clone()
}

// backupConfigFile writes out a backup of current config file if it's bytes are different from newContent
func backupConfigFile(newContent []byte) error {
	const (
		configBackupDir = "config_backups"
		timeLayout      = "2006-01-02T15-04-05"
	)
	configFileAbsPath := viper.ConfigFileUsed()
	currentConfigFileName, currentConfigDir := filepath.Base(configFileAbsPath), filepath.Dir(configFileAbsPath)

	b, err := os.ReadFile(configFileAbsPath)
	if os.IsNotExist(err) {
		logrus.Warn("backupConfigFile(): current configuration file not found")
		return nil
	}
	if err != nil {
		return err
	}

	if bytes.Equal(b, newContent) {
		logrus.Info("backupConfigFile(): aborting, new content is same")
		return nil
	}

	backupDirAbsPath := filepath.Join(currentConfigDir, configBackupDir)
	if err := os.Mkdir(backupDirAbsPath, 0777); err != nil && !os.IsExist(err) {
		return err
	}
	backupFileName := filepath.Join(backupDirAbsPath,
		fmt.Sprintf("%s.%s.bak", currentConfigFileName, time.Now().UTC().Format(timeLayout)))
	return os.WriteFile(backupFileName, b, 0644)
}

// setStandardLoggingLevel sets logging level for logrus standard logger
func setStandardLoggingLevel(level string) error {
	lvl, err := logrus.ParseLevel(level)
	if err != nil {
		return err
	}
	logrus.WithField("level", lvl.String()).Info("global logging")
	logrus.SetLevel(lvl)
	return nil
}

// RegisterCallback f with given name
func RegisterCallback(name string, f func(to *Entire) error) { callbacks.Store(name, f) }

// callback tries to execute callback with name
func callback(name string, to *Entire) error {
	mu.Unlock()
	defer mu.Lock()

	fi, ok := callbacks.Load(name)
	if !ok {
		logrus.Errorf("failed to load configuration callback %s", name)
	}
	logrus.Debugf("calling callback %s", name)
	return fi.(func(to *Entire) error)(to)
}

// applyHotChanges runs appropriate manipulations and callbacks to make new configuration parameters reality
func applyHotChanges(to *Entire) error {
	if current == nil {
		return nil
	}

	// apply new global logging level
	if current.LogLevel != to.LogLevel {
		if err := setStandardLoggingLevel(to.LogLevel); err != nil {
			return err
		}
	}

	hotReloadChecks := []struct {
		currentValue interface{}
		newValue     interface{}
		callbackName string
	}{
		// add new callbacks here
		//{current.Logging.BackgroundLevel, to.Logging.BackgroundLevel, LoggingBackgroundLevelCB},
		//{current.Logging.MetricsLevel, to.Logging.MetricsLevel, LoggingMetricsLevelCB},
	}

	for _, hr := range hotReloadChecks {
		t1, t2 := reflect.TypeOf(hr.currentValue).Name(), reflect.TypeOf(hr.newValue).Name()
		if t1 != t2 {
			return errValuesShouldBeOfOneType
		}
		if !reflect.DeepEqual(hr.currentValue, hr.newValue) {
			if err := callback(hr.callbackName, to); err != nil {
				return err
			}
		}
	}

	return nil
}

// Set the entire current configuration to the copy of a given value.
// If writeOut is set to true, then rewrite the configuration file.
// If runCallbacks is set to true, then run hot-reloading pre-registered callbacks.
// The configuration should be set only via a specific API call.
func Set(to Entire, runCallbacks, writeOut bool) error {
	mu.Lock()
	defer mu.Unlock()

	if runCallbacks {
		if err := applyHotChanges(&to); err != nil {
			return err
		}
	}

	current = &to
	out, err := yaml.Marshal(to)
	if err != nil {
		return err
	}

	if writeOut {
		if err := backupConfigFile(out); err != nil {
			return err
		}
		if err := os.WriteFile(viper.ConfigFileUsed(), out, 0644); err != nil {
			return err
		}
		return viper.ReadInConfig()
	}

	return viper.ReadConfig(bytes.NewReader(out))
}

// SoftSet changes the configuration via f func, but don't writes to a file
func SoftSet(f func(c *Entire)) error {
	clonedConfiguration := Clone()
	f(&clonedConfiguration)
	return Set(clonedConfiguration, true, false)
}
