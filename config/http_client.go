package config

import "time"

// HTTPClient is services configuration section
type HTTPClient struct {
	Timeout int `mapstructure:"timeout" json:"timeout" yaml:"timeout"`
}

// TimeoutDuration returns value of Timeout param as time.Duration
func (c HTTPClient) TimeoutDuration() time.Duration {
	return time.Duration(c.Timeout) * time.Millisecond
}
