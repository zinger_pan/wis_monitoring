package config

// Service is a service definition in the configuration
type Service struct {
	Host string `mapstructure:"host" json:"host" yaml:"host"`
	Path string `mapstructure:"path" json:"path" yaml:"path"`
}
