package config

import "time"

// storage types
const (
	StorageTypeObjectsService = "objects_service"
	StorageTypeMemory         = "memory"
)

// Storage is a storage access configuration
type Storage struct {
	Type          string `mapstructure:"type" json:"type" yaml:"type"`
	Host          string `mapstructure:"host" json:"host" yaml:"host"`
	SlowThreshold int    `mapstructure:"slowThreshold" json:"slowThreshold" yaml:"slowThreshold"`
}

// SlowThresholdDuration returns slowThreshold param value as time.Duration
func (s Storage) SlowThresholdDuration() time.Duration {
	return time.Duration(s.SlowThreshold) * time.Millisecond
}
