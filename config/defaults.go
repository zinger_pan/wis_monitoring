package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// default values for configuration options
const (
	DefaultPort              = 8000
	HTTPClientTimeout        = 5000
	DefaultBatchSize         = 100
	TickersDbCoreNewMessages = 60000
)

// setDefaults initializes viper config with default values
func setDefaults() {
	viper.SetDefault("port", DefaultPort)
	viper.SetDefault("logLevel", logrus.InfoLevel.String())
	viper.SetDefault("httpClient.timeout", HTTPClientTimeout)
	viper.SetDefault("defaultBatchSize", DefaultBatchSize)
	viper.SetDefault("tickers.dbcoreMessagesPollInterval", TickersDbCoreNewMessages)
}
