package config

import "time"

// MTOffsets represents MTOffsets part of the configuration
type MTOffsets struct {
	MeteoTimeExpired       int `mapstructure:"meteoTimeExpired" json:"meteoTimeExpired" yaml:"meteoTimeExpired"`
	BeforeMeteoTimeIndent  int `mapstructure:"beforeMeteoTimeIndent" json:"beforeMeteoTimeIndent" yaml:"beforeMeteoTimeIndent"`
	MTEarlyProtectedPeriod int `mapstructure:"mtEarlyProtectedPeriod" json:"mtEarlyProtectedPeriod" yaml:"mtEarlyProtectedPeriod"`
}

const minMeteoTimeExpired = 1
const minBeforeMeteoTimeIndent = 0
const minMTEarlyProtectedPeriod = 0

// MeteoTimeExpiredDuration returns value of MeteoTimeout param as time.Duration
func (t MTOffsets) MeteoTimeExpiredDuration() time.Duration {
	if t.MeteoTimeExpired < minMeteoTimeExpired {
		t.MeteoTimeExpired = minMeteoTimeExpired
	}
	return time.Duration(t.MeteoTimeExpired) * time.Second
}

// BeforeMeteoTimeIndentDuration returns value of BeforeMeteoTimeIndent param as time.Duration
func (t MTOffsets) BeforeMeteoTimeIndentDuration() time.Duration {
	if t.BeforeMeteoTimeIndent < minBeforeMeteoTimeIndent {
		t.BeforeMeteoTimeIndent = minBeforeMeteoTimeIndent
	}
	return time.Duration(t.MeteoTimeExpired) * time.Second
}

// BeforeMeteoTimeIndentDuration returns value of BeforeMeteoTimeIndent param as time.Duration
func (t MTOffsets) MTEarlyProtectedPeriodDuration() time.Duration {
	if t.MTEarlyProtectedPeriod < minMTEarlyProtectedPeriod {
		t.MTEarlyProtectedPeriod = minMTEarlyProtectedPeriod
	}
	return time.Duration(t.MTEarlyProtectedPeriod) * time.Second
}
