package config

import "time"

// Tickers represents tickers part of the configuration
type Tickers struct {
	DbCoreMessagesPollInterval int `mapstructure:"dbcoreMessagesPollInterval" json:"dbcoreMessagesPollInterval" yaml:"dbcoreMessagesPollInterval"`
}

const minDbCoreMessagesPollInterval = 1000

// DbCoreMessagesPollIntervalDuration returns value of DbCoreMessagesPollInterval param as time.Duration
func (t Tickers) DbCoreMessagesPollIntervalDuration() time.Duration {
	if t.DbCoreMessagesPollInterval < minDbCoreMessagesPollInterval {
		t.DbCoreMessagesPollInterval = minDbCoreMessagesPollInterval
	}
	return time.Duration(t.DbCoreMessagesPollInterval) * time.Millisecond
}
