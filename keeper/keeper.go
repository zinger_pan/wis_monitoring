package keeper

import (
	"context"

	"github.com/go-openapi/strfmt"
	"monitoring-for-wis/keeper/models"
)

// Keeper abstracts a storage, like a database
type Keeper interface {
	Close() error
	DeleteAll(context.Context) error

	List(context.Context, models.ObjectsServiceListParams) ([]models.ObjectsServiceObject, int, error)
	Get(context.Context, strfmt.UUID) (*models.ObjectsServiceObject, error)
	Create(context.Context, []models.ObjectsServiceObject) ([]strfmt.UUID, error)
	Update(context.Context, strfmt.UUID, map[string]interface{}) error
	Archive(context.Context, strfmt.UUID) error
	Restore(context.Context, strfmt.UUID) error
	Remove(context.Context, strfmt.UUID) error
}
