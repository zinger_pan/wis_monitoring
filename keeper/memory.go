package keeper

import (
	"context"
	"sync"
	"time"

	"monitoring-for-wis/keeper/models"

	"github.com/AlekSi/pointer"
	"github.com/go-openapi/strfmt"
	"github.com/mtfelian/utils"
	uuid "github.com/satori/go.uuid"
)

// Memory implements Keeper with in-memory storage.
// Useful for tests without launching the actual DB instance.
type Memory struct {
	objects   map[strfmt.UUID]models.ObjectsServiceObject
	contentMu sync.Mutex
}

// Close is a placeholder in this implementation
func (s *Memory) Close() error { return nil }

// DeleteAll is used before/after each test
func (s *Memory) DeleteAll(ctx context.Context) error {
	s.objects = make(map[strfmt.UUID]models.ObjectsServiceObject)
	return nil
}

// NewMemory returns a new instance of Memory
func NewMemory() *Memory {
	return &Memory{
		objects: make(map[strfmt.UUID]models.ObjectsServiceObject),
	}
}

// CreateContent in the storage
func (s *Memory) Create(ctx context.Context, objs []models.ObjectsServiceObject) ([]strfmt.UUID, error) {
	s.contentMu.Lock()
	defer s.contentMu.Unlock()
	UUIDs := make([]strfmt.UUID, 0)
	for _, obj := range objs {
		if len(obj.ID) == 0 {
			obj.ID = strfmt.UUID(uuid.NewV4().String())
		}

		if _, ok := s.objects[obj.ID]; ok { // if ID of passed content already exists in storage
			return nil, errContentIDAlreadyExists
		}

		now := time.Now().UTC()
		obj.CreatedAt, obj.UpdatedAt = now.Unix(), now.Unix()
		s.objects[obj.ID] = obj
		UUIDs = append(UUIDs, obj.ID)
	}

	return UUIDs, nil
}

// UpdateContent in the storage
func (s *Memory) Update(ctx context.Context, ID strfmt.UUID, fields map[string]interface{}) error {
	if len(fields) == 0 {
		return nil
	}
	s.contentMu.Lock()
	defer s.contentMu.Unlock()

	obj, ok := s.objects[ID]
	if !ok {
		return errRecordNotFound
	}

	if err := obj.FillFields(fields); err != nil {
		return err
	}
	now := time.Now().UTC()
	obj.UpdatedAt = now.Unix()
	s.objects[ID] = obj

	return nil
}

// ListContent from the storage
func (s *Memory) List(ctx context.Context, objListParam models.ObjectsServiceListParams) (items []models.ObjectsServiceObject, total int, err error) {
	s.contentMu.Lock()
	defer s.contentMu.Unlock()

	//objParam := toObjectsServiceListParams(data)
	updFrom := time.Unix(objListParam.UpdatedFrom, 0)
	//updTo := time.Unix(objParam.UpdateTo, 0)

	flagArchivreOnly := utils.SliceContains("archiveonly", objListParam.Flags)
	flagAll := utils.SliceContains("all", objListParam.Flags)

	//p := data.(models.ListContentParams)
	items = make([]models.ObjectsServiceObject, 0, len(s.objects))
	for _, content := range s.objects {
		updAt := time.Unix(content.CreatedAt, 0)
		var archived time.Time
		if content.Archived != nil {
			archived = time.Unix(*content.Archived, 0)
		}

		if (len(objListParam.IDs) > 0 && !utils.SliceContains(content.ID, objListParam.IDs)) ||
			(len(objListParam.Owner) > 0 && (!content.Owner.Valid || !utils.SliceContains(content.Owner.ToUUID(), objListParam.Owner))) ||
			(!updFrom.IsZero() && updAt.Before(updFrom)) ||
			(flagArchivreOnly && content.Archived == nil) ||
			(!flagAll && content.Archived != nil && !archived.IsZero()) {
			continue
		}
		items = append(items, content)
	}
	total = len(items) // pagination is not implemented, so it is
	return
}

// GetContent with ID given
func (s *Memory) Get(ctx context.Context, ID strfmt.UUID) (*models.ObjectsServiceObject, error) {
	s.contentMu.Lock()
	defer s.contentMu.Unlock()
	content, ok := s.objects[ID]
	if !ok {
		return nil, nil
	}
	return &content, nil
}

// ArchiveContent in the storage
func (s *Memory) Archive(ctx context.Context, ID strfmt.UUID) error {
	s.contentMu.Lock()
	defer s.contentMu.Unlock()
	now := models.TimeNow()
	if content, ok := s.objects[ID]; ok && content.Archived == nil {
		content := s.objects[ID]
		content.UpdatedAt, content.Archived = now.Unix(), pointer.ToInt64(now.Unix())
		s.objects[ID] = content
	}
	return nil
}

// RestoreContent in the storage
func (s *Memory) Restore(ctx context.Context, ID strfmt.UUID) error {
	s.contentMu.Lock()
	defer s.contentMu.Unlock()
	now := models.TimeNow()
	if content, ok := s.objects[ID]; ok && content.Archived != nil {
		content := s.objects[ID]
		content.UpdatedAt, content.Archived = now.Unix(), nil
		s.objects[ID] = content
	}
	return nil
}

// RemoveContent from the storage
func (s *Memory) Remove(ctx context.Context, ID strfmt.UUID) error {
	s.contentMu.Lock()
	defer s.contentMu.Unlock()
	content, ok := s.objects[ID]
	if ok && content.Archived != nil { // we skip non-archived content
		delete(s.objects, ID)
	}
	return nil
}
