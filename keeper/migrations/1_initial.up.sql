DO LANGUAGE plpgsql
$$
    BEGIN
        EXECUTE format('ALTER DATABASE %s SET timezone TO "UTC"', current_database()::TEXT);
    END;
$$;

CREATE TABLE IF NOT EXISTS rules
(
    urn      TEXT                                           NOT NULL
        CONSTRAINT rules_pk PRIMARY KEY,
    ct       TIMESTAMP DEFAULT timezone('utc'::TEXT, now()) NOT NULL,
    ut       TIMESTAMP DEFAULT timezone('utc'::TEXT, now()) NOT NULL,
    dt       TIMESTAMP,
    schedule JSONB                                          NOT NULL,
    regexp   TEXT                                           NOT NULL
);

CREATE TABLE IF NOT EXISTS metrics
(
    urn     TEXT                                           NOT NULL,
    date    TIMESTAMP                                      NOT NULL,
    ct      TIMESTAMP DEFAULT timezone('utc'::TEXT, now()) NOT NULL,
    ut      TIMESTAMP DEFAULT timezone('utc'::TEXT, now()) NOT NULL,
    dt      TIMESTAMP,
    metrics JSONB                                          NOT NULL,
    PRIMARY KEY (urn, date)
);
