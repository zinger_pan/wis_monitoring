package keeper

import "errors"

var (
	errRecordNotFound         = errors.New("record not found")
	errContentIDAlreadyExists = errors.New("content ID already exists")
)
