package keeper

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"monitoring-for-wis/keeper/models"

	"github.com/go-openapi/strfmt"
	"github.com/mtfelian/utils"
	"github.com/spf13/viper"
)

// ObjectsService implements Keeper with in-objectsService storage.
// Useful for tests without launching the actual DB instance.
type ObjectsService struct {
	host   string
	client *http.Client
}

// ObjectsServiceParams for creating the entity
type ObjectsServiceParams struct {
	Host    string
	Timeout time.Duration
}

// NewTestsObjectsServiceParams returns ObjectsService params for tests
func NewTestsObjectsServiceParams() ObjectsServiceParams {
	return ObjectsServiceParams{
		Host:    viper.GetString("storage.host"),
		Timeout: time.Second,
	}
}

// Close is a placeholder in this implementation
func (s *ObjectsService) Close() error { return nil }

// DeleteAll is used before/after each test
func (s *ObjectsService) DeleteAll(ctx context.Context) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodDelete,
		fmt.Sprintf("%s/api/v1/objects/drop?type=%s", s.host, models.ObjectMonitoringType), nil)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	req, err = http.NewRequestWithContext(ctx, http.MethodDelete,
		fmt.Sprintf("%s/api/v1/objects/drop?type=%s", s.host, models.ObjectSignalsType), nil)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err = s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

// NewObjectsService returns a new instance of ObjectsService
func NewObjectsService(p ObjectsServiceParams) *ObjectsService {
	return &ObjectsService{
		host:   p.Host,
		client: &http.Client{Timeout: p.Timeout},
	}
}

// Create in the storage
func (s *ObjectsService) Create(ctx context.Context, obj []models.ObjectsServiceObject) ([]strfmt.UUID, error) {
	body := utils.MushMarshalJSON(struct {
		Objects []models.ObjectsServiceObject `json:"objects"`
	}{Objects: obj})
	req, err := http.NewRequestWithContext(ctx, http.MethodPost,
		fmt.Sprintf("%s/api/v1/objects/new", s.host), bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var b []byte
	if b, err = io.ReadAll(resp.Body); err != nil {
		return nil, err
	}
	var apiResponse struct {
		IDs []strfmt.UUID `json:"ids"`
	}
	if err = json.Unmarshal(b, &apiResponse); err != nil {
		return nil, err
	}

	return apiResponse.IDs, nil
}

// Update in the storage
func (s *ObjectsService) Update(ctx context.Context, ID strfmt.UUID, fields map[string]interface{}) error {
	data := utils.MushMarshalJSON(struct {
		Objects map[strfmt.UUID]map[string]interface{} `json:"objects"`
	}{Objects: map[strfmt.UUID]map[string]interface{}{ID: fields}})
	req, err := http.NewRequestWithContext(ctx, http.MethodPost,
		fmt.Sprintf("%s/api/v1/objects/update", s.host), bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

// List from the storage
func (s *ObjectsService) List(ctx context.Context, objListParams models.ObjectsServiceListParams) (content []models.ObjectsServiceObject, total int, err error) {
	body := utils.MushMarshalJSON(objListParams)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost,
		fmt.Sprintf("%s/api/v1/objects/list", s.host), bytes.NewReader(body))
	if err != nil {
		return nil, 0, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()
	var b []byte
	if b, err = io.ReadAll(resp.Body); err != nil {
		return nil, 0, err
	}
	var apiResponse struct {
		Info struct {
			Total int `json:"total"`
		} `json:"info"`
		Objects []models.ObjectsServiceObject `json:"objects"`
	}
	if err = json.Unmarshal(b, &apiResponse); err != nil {
		return nil, 0, err
	}

	return apiResponse.Objects, apiResponse.Info.Total, nil
}

// Get with ID given
func (s *ObjectsService) Get(ctx context.Context, ID strfmt.UUID) (*models.ObjectsServiceObject, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf("%s/api/v1/objects/%s", s.host, ID), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := s.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNotFound {
		return nil, err
	}

	var b []byte
	if b, err = io.ReadAll(resp.Body); err != nil {
		return nil, err
	}
	var apiResponse struct {
		Object models.ObjectsServiceObject `json:"object"`
	}
	if err = json.Unmarshal(b, &apiResponse); err != nil {
		return nil, err
	}
	//content := apiResponse.Object.ToContent()
	return &apiResponse.Object, nil
}

// Archive in the storage
func (s *ObjectsService) Archive(ctx context.Context, ID strfmt.UUID) error {
	data := []byte(fmt.Sprintf(fmt.Sprintf(`{"ids":[%q]}`, ID)))
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/api/v1/objects/archive", s.host),
		bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

// Restore in the storage
func (s *ObjectsService) Restore(ctx context.Context, ID strfmt.UUID) error {
	data := []byte(fmt.Sprintf(fmt.Sprintf(`{"ids":[%q]}`, ID)))
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/api/v1/objects/restore", s.host),
		bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

// Remove from the storage
func (s *ObjectsService) Remove(ctx context.Context, ID strfmt.UUID) error {
	data := []byte(fmt.Sprintf(fmt.Sprintf(`{"ids":[%q]}`, ID)))
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/api/v1/objects/remove", s.host),
		bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}
