package keeper_test

import (
	"context"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/go-openapi/strfmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"monitoring-for-wis/config"
	"monitoring-for-wis/keeper"
	"monitoring-for-wis/keeper/models"
)

func timeMustParse(t string) time.Time {
	res, err := time.Parse("2006-01-02T15:04:05", t)
	if err != nil {
		panic(err)
	}
	return res
}

var _ = Describe("tests for the storage", func() {
	var storage keeper.Keeper

	BeforeSuite(func() {
		logrus.SetLevel(logrus.DebugLevel)
	})
	AfterSuite(func() {})

	var (
		now             time.Time
		rulesToCreate   = []models.Rule{}
		metricsToCreate = []models.MetricMetrics{}
		objectsToCreate = []models.ObjectsServiceObject{}
		ctx             = context.Background()

		ID1 = strfmt.UUID(uuid.NewV4().String())
		ID2 = strfmt.UUID(uuid.NewV4().String())
	)

	const (
		urn1 = "test:urn:1"
		urn2 = "test:urn:2"
		urn3 = "test:urn:3"
	)

	BeforeEach(func() {
		now = time.Now().Truncate(time.Millisecond).UTC()
		rulesToCreate = []models.Rule{
			{
				ID:        ID1,
				URN:       urn1,
				CreatedAt: now,
				UpdatedAt: now,
				Schedule:  "every 30 min",
				RegExp:    "",
			},
			{
				ID:        ID2,
				URN:       urn2,
				CreatedAt: now,
				UpdatedAt: now,
				Schedule:  "every 60 min",
				RegExp:    "",
			},
		}

		metricsToCreate = []models.MetricMetrics{
			{
				Terms: []models.MetricMetricsTermsItem{
					{
						MeteoTerm:   strfmt.DateTime(timeMustParse("2021-06-18T14:00:00")),
						Requirement: 0,
						Created:     strfmt.DateTime(timeMustParse("2021-06-18T14:33:00")),
						Updated:     strfmt.DateTime(timeMustParse("2021-06-18T14:33:00")),
					},
				},
			},
			{
				Terms: []models.MetricMetricsTermsItem{
					{
						MeteoTerm:   strfmt.DateTime(timeMustParse("2021-06-18T14:10:00")),
						Requirement: 1,
						Created:     strfmt.DateTime(timeMustParse("2021-06-18T14:34:00")),
						Updated:     strfmt.DateTime(timeMustParse("2021-06-18T14:34:00")),
					},
				},
			},
		}

		objectsToCreate = []models.ObjectsServiceObject{
			rulesToCreate[0].ToObjectsServiceObject(),
			rulesToCreate[1].ToObjectsServiceObject(),
		}

		viper.SetConfigType("yml")
	})

	for _, storageType := range []string{config.StorageTypeMemory} {

		Context(fmt.Sprintf("with storage type %s", storageType), func() {
			BeforeEach(func() {
				switch storageType {
				case config.StorageTypeMemory:
					storage = keeper.NewMemory()
				case config.StorageTypeObjectsService:
					var err error
					storage = keeper.NewObjectsService(keeper.NewTestsObjectsServiceParams())
					Expect(err).NotTo(HaveOccurred())
				}
				Expect(storage).NotTo(BeNil())
				Expect(storage.DeleteAll(context.Background())).To(Succeed())

			})

			It("checks closing keeper", func() {
				Expect(storage.Close()).To(Succeed())
			})

			Context("rules API", func() {
				BeforeEach(func() {
					By("adding rules", func() {
						UUIDs, err := storage.Create(ctx, objectsToCreate)
						Expect(err).NotTo(HaveOccurred())
						Expect(UUIDs).To(HaveLen(len(objectsToCreate)))

					})
				})

				It("checks listing rules", func() {
					rules, count, err := storage.List(ctx, models.ObjectsServiceListParams{})
					Expect(err).NotTo(HaveOccurred())
					Expect(count).To(Equal(len(objectsToCreate)))
					Expect(rules).To(HaveLen(len(objectsToCreate)))
				})

				It("checks updating rule", func() {
					rule := rulesToCreate[0]
					rule.Metrics = metricsToCreate[0]
					By("updating", func() {

						fields := map[string]interface{}{
							models.ObjectFieldData: rule.ToObjectData(),
						}
						Expect(storage.Update(ctx, rule.ID, fields)).To(Succeed())
					})
					By("getting", func() {
						obj, err := storage.Get(ctx, rulesToCreate[0].ID)
						Expect(err).NotTo(HaveOccurred())
						Expect(obj.R1).To(Equal(urn1))
						Expect(obj.Data).To(MatchJSON(rule.ToObjectData()))
					})

					By("listing", func() {
						objs, _, err := storage.List(ctx, models.ObjectsServiceListParams{IDs: []strfmt.UUID{rulesToCreate[0].ID}})
						Expect(err).NotTo(HaveOccurred())
						Expect(objs).To(HaveLen(1))
						Expect(objs[0].R1).To(Equal(urn1))
						Expect(objs[0].Data).To(MatchJSON(rule.ToObjectData()))
					})
				})
			})
		})
	}
})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "Keeper Suite")
}
