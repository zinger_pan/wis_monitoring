package models

import (
	"fmt"
	"regexp"
	"strconv"
	"time"

	"github.com/go-openapi/strfmt"
)

var (
	ErrParseHeaderStr     = "couldn't parse header for jrid %s"
	ErrParseMessageFMTStr = "couldn't parse fmt for jrid %s"
)

// MonitoringData represents monitoringData storage model
// JournalRecord is a DbCore JournalRecord model
type JournalRecord struct {
	JRID       strfmt.UUID   `json:"jrid"`
	Key        string        `json:"key"`
	Name       string        `json:"name"`
	TimeStamp  int64         `json:"time"`
	First      string        `json:"first"`
	Attributes interface{}   `json:"attrs"`
	BodyType   string        `json:"bt"`
	Size       int64         `json:"size"`
	Hash       string        `json:"hash"`
	Priority   int           `json:"priority"`
	Ext        string        `json:"ext"`
	Type       string        `json:"type"`
	Body       strfmt.Base64 `json:"body"`
	FileLink   struct {
		FileID strfmt.UUID `json:"link_id"`
	} `json:"file_link"`
	URN      string        `json:"urn"`
	Cmpid    []strfmt.UUID `json:"cmpid"`
	HaveBody bool          `json:"have_body"`
	FmtTime  time.Time     `json:"-"`
}

// ListJournalRecordResponse represents a response for ListJournalRecords method
type ListJournalRecordResponse struct {
	Records struct {
		Count uint            `json:"count"`
		Items []JournalRecord `json:"items"`
	} `json:"jrecs"`
}

func FindJournalRecordByKey(records []JournalRecord, key string) *JournalRecord {
	for _, jr := range records {
		if key == jr.Key {
			return &jr
		}
	}
	return nil
}

func daysIn(m time.Month, year int) int {
	// This is equivalent to time.daysIn(m, year).
	return time.Date(year, m+1, 0, 0, 0, 0, 0, time.UTC).Day()
}

func (jr *JournalRecord) Fmt() (time.Time, error) {
	if !jr.FmtTime.IsZero() {
		return jr.FmtTime, nil
	}

	jrTime := time.Unix(jr.TimeStamp, 0).UTC()
	re := regexp.MustCompile(`\w+ \w+ (\d{2})(\d{2})(\d{2})`)
	hfmt := re.FindStringSubmatch(jr.Name)

	YY, err := strconv.Atoi(hfmt[1]) // day
	if err != nil {
		return time.Time{}, fmt.Errorf(ErrParseHeaderStr, jr.JRID)
	}
	gg, err := strconv.Atoi(hfmt[2]) // hour
	if err != nil {
		return time.Time{}, fmt.Errorf(ErrParseHeaderStr, jr.JRID)
	}
	n, err := strconv.Atoi(hfmt[3]) // minutes
	if err != nil {
		return time.Time{}, fmt.Errorf(ErrParseHeaderStr, jr.JRID)
	}
	AYear, AMon, DD := jrTime.Year(), jrTime.Month(), jrTime.Day()
	period := 3 * (31 - DD) % 4

	// 1. Сводка за сегодня или прошедшие дни этого месяца?
	if YY <= DD {
		jr.FmtTime = time.Date(AYear, AMon, YY, gg, n, 0, 0, time.UTC)
		return jr.FmtTime, nil
	}
	// 2. Сводка за "завтра" ? этот вариант проверяется во вторую очередь
	// если число = завтрашнему, то вероятнее что прислали телеграмму немного раньше срока, чем месячной давности
	DT := jrTime.AddDate(0, 0, 1)
	AYear, AMon, DD = DT.Year(), DT.Month(), DT.Day()
	if YY == DD {
		jr.FmtTime = time.Date(AYear, AMon, YY, gg, n, 0, 0, time.UTC)
		return jr.FmtTime, nil
	}
	// 3. Сводка за последние числа прошлого месяца ? проверяем в последнюю очередь
	// определяем допустимый диапазон чисел в прошлом месяце
	DT = jrTime.AddDate(0, -1, 0)
	AYear, AMon = DT.Year(), DT.Month()
	DD = daysIn(AMon, AYear)
	if YY >= DD-period && YY <= DD {
		jr.FmtTime = time.Date(AYear, AMon, YY, gg, n, 0, 0, time.UTC)
		return jr.FmtTime, nil
	}

	return time.Time{}, fmt.Errorf(ErrParseMessageFMTStr, jr.JRID)

}
