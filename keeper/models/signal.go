package models

import (
	"strings"
	"time"

	"github.com/AlekSi/pointer"
	"github.com/go-openapi/strfmt"
	"gorm.io/datatypes"
)

// Signal represents signal storage model
type Signal struct {
	ID        strfmt.UUID
	CompanyID NullUUID
	Kind      string
	Caption   string
	Msg       string
	Comment   string
	CreatedAt time.Time
	UpdatedAt time.Time
	Archived  *time.Time
	Data      datatypes.JSON
}

func (s Signal) ToObjectsServiceObject() (res ObjectsServiceObject) {
	res = ObjectsServiceObject{
		ID:        s.ID,
		Owner:     s.CompanyID,
		R1:        s.Kind,
		Name:      s.Caption,
		R2:        s.Msg,
		R3:        s.Comment,
		CreatedAt: s.CreatedAt.Unix(),
		UpdatedAt: s.UpdatedAt.Unix(),

		Data: s.Data,
		Type: ObjectSignalsType,
	}
	if s.Archived != nil && !s.Archived.IsZero() {
		res.Archived = pointer.ToInt64(s.Archived.Unix())
	}
	return
}

// ListSignalParams for listing signal
type ListSignalParams struct {
	Page            int
	PageSize        int
	IDs             []strfmt.UUID
	CompanyIDs      []strfmt.UUID
	Kinds           []string
	UpdatedFrom     time.Time
	UpdatedTo       time.Time
	SearchStr       string
	Search          []string
	Order           []string
	OrderDir        string
	FlagAll         bool
	FlagArchiveOnly bool
}

// ToObjectsServiceListParams converts ListStationsParams to ObjectsServiceListParams
func (p ListSignalParams) ToObjectsServiceListParams() (res ObjectsServiceListParams) {
	res = ObjectsServiceListParams{
		Page:      p.Page,
		PageSize:  p.PageSize,
		IDs:       p.IDs,
		Owner:     p.CompanyIDs,
		R1:        p.Kinds,
		Order:     p.Order,
		OrderDir:  p.OrderDir,
		SearchStr: p.SearchStr,
		Type:      []string{ObjectSignalsType},
	}
	if !p.UpdatedFrom.IsZero() {
		res.UpdatedFrom = p.UpdatedFrom.Unix()
	}

	if !p.UpdatedTo.IsZero() {
		res.UpdateTo = p.UpdatedTo.Unix()
	}

	for _, s := range p.Search {
		if strings.EqualFold(s, "caption") {
			res.SearchBy = append(res.SearchBy, "name")
		}

		if strings.EqualFold(s, "kind") {
			res.SearchBy = append(res.SearchBy, "r1")
		}
	}

	if p.FlagAll {
		res.Flags = append(res.Flags, "all")
	}
	if p.FlagArchiveOnly {
		res.Flags = append(res.Flags, "archiveonly")
	}
	return
}
