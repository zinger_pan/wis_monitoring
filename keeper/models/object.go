package models

import (
	"fmt"
	"time"

	"github.com/AlekSi/pointer"
	"github.com/go-openapi/strfmt"
	"github.com/mtfelian/utils"
	"gorm.io/datatypes"
)

const (
	ObjectMonitoringType = "MON_RULES"
	ObjectSignalsType    = "MON_SIG"
	ObjectFieldData      = "data"
	ObjectFieldName      = "name"
	ObjectFieldProp1     = "r1"
	ObjectFieldProp2     = "r2"
	ObjectFieldProp3     = "r3"
)

// ObjectsServiceObject is an API model for objects service
type ObjectsServiceObject struct {
	ID        strfmt.UUID    `json:"id"`
	CreatedAt int64          `json:"created"`
	UpdatedAt int64          `json:"updated"`
	Owner     NullUUID       `json:"owner"`
	R1        string         `json:"r1"`
	R2        string         `json:"r2"`
	R3        string         `json:"r3"`
	Name      string         `json:"name"`
	Archived  *int64         `json:"archived"`
	Data      datatypes.JSON `json:"data"`
	Type      string         `json:"type"` // w, should be set to "content"
}

func (o ObjectsServiceObject) ToRule() (res Rule) {
	data := new(RuleData)
	utils.MustMarshalUnmarshalJSON(o.Data, data)
	res = Rule{
		ID:        o.ID,
		Name:      o.Name,
		URN:       o.R1,
		CompanyID: o.Owner,
		CreatedAt: time.Unix(o.CreatedAt, 0),
		UpdatedAt: time.Unix(o.UpdatedAt, 0),
		RegExp:    data.RegExp,
		Schedule:  data.Schedule,
		Metrics:   data.Metric,
	}
	if o.Archived != nil {
		res.Archived = pointer.ToTime(time.Unix(*o.Archived, 0))
	}

	return
}

func (o ObjectsServiceObject) ToSignal() (res Signal) {
	res = Signal{
		ID:        o.ID,
		CompanyID: o.Owner,
		Kind:      o.R1,
		Caption:   o.Name,
		Msg:       o.R2,
		Comment:   o.R3,
		CreatedAt: time.Unix(o.CreatedAt, 0),
		UpdatedAt: time.Unix(o.UpdatedAt, 0),
		Data:      o.Data,
	}
	if o.Archived != nil {
		res.Archived = pointer.ToTime(time.Unix(*o.Archived, 0))
	}

	return
}

// FillFields from map m into content
func (o *ObjectsServiceObject) FillFields(m map[string]interface{}) error {
	var ok bool
	for key, value := range m {
		switch key {
		case ObjectFieldName:
			if o.Name, ok = value.(string); !ok {
				return fmt.Errorf("name field is not of type string")
			}
		case ObjectFieldData:
			if o.Data, ok = value.(datatypes.JSON); !ok {
				return fmt.Errorf("data field is not of type datatypes.JSON")
			}
		}
	}
	return nil
}

// ObjectsServiceListParams is a type for objects service API
type ObjectsServiceListParams struct {
	Page        int           `json:"page,omitempty"`
	PageSize    int           `json:"pageSize,omitempty"`
	IDs         []strfmt.UUID `json:"id,omitempty"`
	Owner       []strfmt.UUID `json:"owner,omitempty"`
	Order       []string      `json:"order,omitempty"`
	OrderDir    string        `json:"orderDir,omitempty"`
	R1          []string      `json:"r1,omitempty"`
	R2          []string      `json:"r2,omitempty"`
	R3          []string      `json:"r3,omitempty"`
	UpdatedFrom int64         `json:"updatedFrom,omitempty"`
	UpdateTo    int64         `json:"updatedTo,omitempty"`
	SearchStr   string        `json:"searchStr,omitempty"`
	SearchBy    []string      `json:"searchBy,omitempty"`
	Flags       []string      `json:"flags,omitempty"`
	Type        []string      `json:"type"`
}
