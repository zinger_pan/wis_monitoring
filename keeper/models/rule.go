package models

import (
	"regexp"
	"strings"
	"time"

	"github.com/AlekSi/pointer"
	"github.com/go-openapi/strfmt"
	"github.com/mtfelian/utils"
	"gorm.io/datatypes"
)

// Rule represents rule storage model
type Rule struct {
	ID        strfmt.UUID
	CompanyID NullUUID
	URN       string
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	Archived  *time.Time

	Schedule       string
	RegExp         string
	Metrics        MetricMetrics
	RegExpCompiled *regexp.Regexp `gorm:"-"`
}

type RuleData struct {
	Schedule string        `json:"schedule"`
	RegExp   string        `json:"regexp"`
	Metric   MetricMetrics `json:"metrics"`
}

func (r Rule) ToObjectData() datatypes.JSON {

	d := RuleData{
		Schedule: r.Schedule,
		RegExp:   r.RegExp,
		Metric:   r.Metrics,
	}

	return utils.MushMarshalJSON(d)
}

func (r Rule) ToObjectsServiceObject() (res ObjectsServiceObject) {
	res = ObjectsServiceObject{
		ID:        r.ID,
		Owner:     r.CompanyID,
		Name:      r.Name,
		CreatedAt: r.CreatedAt.Unix(),
		UpdatedAt: r.UpdatedAt.Unix(),
		R1:        r.URN,
		Data:      r.ToObjectData(),
		Type:      ObjectMonitoringType,
	}
	if r.Archived != nil && !r.Archived.IsZero() {
		res.Archived = pointer.ToInt64(r.Archived.Unix())
	}
	return
}

// ListRulesParams for listing rules
type ListRulesParams struct {
	Page            int
	PageSize        int
	IDs             []strfmt.UUID
	CompanyIDs      []strfmt.UUID
	URNs            []string
	UpdatedFrom     time.Time
	UpdatedTo       time.Time
	SearchStr       string
	Search          []string
	Order           []string
	OrderDir        string
	FlagAll         bool
	FlagArchiveOnly bool
}

// ToObjectsServiceListParams converts ListStationsParams to ObjectsServiceListParams
func (p ListRulesParams) ToObjectsServiceListParams() (res ObjectsServiceListParams) {
	res = ObjectsServiceListParams{
		Page:      p.Page,
		PageSize:  p.PageSize,
		IDs:       p.IDs,
		Owner:     p.CompanyIDs,
		Order:     p.Order,
		OrderDir:  p.OrderDir,
		R1:        p.URNs,
		SearchStr: p.SearchStr,
		Type:      []string{ObjectMonitoringType},
	}
	if !p.UpdatedFrom.IsZero() {
		res.UpdatedFrom = p.UpdatedFrom.Unix()
	}

	if !p.UpdatedTo.IsZero() {
		res.UpdateTo = p.UpdatedTo.Unix()
	}

	for _, s := range p.Search {
		if strings.EqualFold(s, "name") {
			res.SearchBy = append(res.SearchBy, "name")
		}

		if strings.EqualFold(s, "urn") {
			res.SearchBy = append(res.SearchBy, "r1")
		}
	}

	if p.FlagAll {
		res.Flags = append(res.Flags, "all")
	}
	if p.FlagArchiveOnly {
		res.Flags = append(res.Flags, "archiveonly")
	}
	return
}

// Clone returns a full copy of the rule
func (r Rule) Clone() Rule {
	newRule := r // this copies all by-value fields
	if r.RegExpCompiled != nil {
		newRule.RegExpCompiled = regexp.MustCompile(r.RegExp)
	}
	return newRule
}

func TimeNow() time.Time { return time.Now().Truncate(time.Millisecond).UTC() }
