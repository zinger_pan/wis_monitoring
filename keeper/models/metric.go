package models

import (
	"encoding/json"
	"time"

	"github.com/go-openapi/strfmt"
)

// MetricMetricsTermsItem describes an item of Terms array in Metric.Metrics object
type MetricMetricsTermsItem struct {
	MeteoTerm   strfmt.DateTime `json:"mt"`
	Requirement int             `json:"type"` // metric type: data not arrived, arrived properly, earlier than expected etc
	Created     strfmt.DateTime `json:"stor_ct"`
	Updated     strfmt.DateTime `json:"stor_ut"`

	JRTime time.Time `json:"-"` // for convenience of passing the value
}

// MetricMetrics describes Metric.Metrics field
type MetricMetrics struct {
	Terms []MetricMetricsTermsItem `json:"terms"`
}

// TermIndex returns index of the entry in the Terms field if found, otherwise returns -1
func (mm MetricMetrics) TermIndex(meteoTerm time.Time) int {
	for i, term := range mm.Terms {
		if time.Time(term.MeteoTerm).Equal(meteoTerm) {
			return i
		}
	}
	return -1
}

func newEmptyMetricMetrics() MetricMetrics { return MetricMetrics{Terms: []MetricMetricsTermsItem{}} }

// NewEmptyMetricMetricsJSON returns empty JSON representation of MetricMetrics object
func NewEmptyMetricMetricsJSON() []byte {
	b, err := json.Marshal(newEmptyMetricMetrics())
	if err != nil {
		panic(err)
	}
	return b
}
