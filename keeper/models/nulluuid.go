package models

import (
	"database/sql/driver"
	"encoding/json"

	"github.com/go-openapi/strfmt"
)

// ZeroUUID is a constant value indicating zero UUID value
const ZeroUUID strfmt.UUID = "00000000-0000-0000-0000-000000000000"

// NullUUID represents a strfmt.UUID that may be null.
// NullUUID implements the Scanner interface so
// it can be used as a scan destination:
//
//  var s NullString
//  err := db.QueryRow("SELECT some_id FROM foo WHERE id=?", id).Scan(&s)
//  ...
//  if s.Valid {
//     // use s.UUID
//  } else {
//     // NULL value
//  }
//
type NullUUID struct {
	UUID  strfmt.UUID
	Valid bool // Valid is true if String is not NULL
}

// UnmarshalJSON makes NullUUID to implement json.Unmarshaller
func (nu *NullUUID) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		return nil
	}
	if err := json.Unmarshal(data, &nu.UUID); err != nil {
		return err
	}
	nu.Valid = true
	return nil
}

// MarshalJSON makes NullUUID to implement json.Mashaller
func (nu NullUUID) MarshalJSON() ([]byte, error) {
	if nu.Valid && len(nu.UUID) > 0 {
		return json.Marshal(nu.UUID)
	}
	return json.Marshal(nil)
}

// Scan implements the Scanner interface.
func (nu *NullUUID) Scan(value interface{}) error {
	if value == nil {
		nu.UUID, nu.Valid = "", false
		return nil
	}
	if err := nu.UUID.Scan(value); err != nil {
		return err
	}
	nu.Valid = true
	return nil
}

// Value implements the driver Valuer interface.
func (nu NullUUID) Value() (driver.Value, error) {
	if !nu.Valid || len(nu.UUID) == 0 {
		return nil, nil
	}
	return nu.UUID.String(), nil
}

// String returns string representation
func (nu NullUUID) String() string {
	if nu.Valid && len(nu.UUID) > 0 {
		return nu.UUID.String()
	}
	return ""
}

// ToUUIDPtr returns nil or strfmt.UUID pointer
func (nu NullUUID) ToUUIDPtr() *strfmt.UUID {
	if !nu.Valid || len(nu.UUID) == 0 {
		return nil
	}
	return &nu.UUID
}

// ToUUID returns strfmt.UUID
func (nu NullUUID) ToUUID() strfmt.UUID {
	if !nu.Valid || len(nu.UUID) == 0 {
		return ZeroUUID
	}
	return nu.UUID
}

// IsZero returns whether UUID is of empty or zero value
func (nu NullUUID) IsZero() bool {
	return !nu.Valid || nu.UUID == "" || len(nu.UUID) == 0 || nu.UUID == ZeroUUID
}

// NewNullUUIDFromUUIDPtr converts uuid pointer to NullUUID
func NewNullUUIDFromUUIDPtr(uuid *strfmt.UUID) NullUUID {
	if uuid == nil || len(*uuid) == 0 {
		return NullUUID{}
	}
	return NullUUID{Valid: true, UUID: *uuid}
}

// NewNullUUIDFromUUID creates a new NullUUID from strfmt.UUID
func NewNullUUIDFromUUID(uuid strfmt.UUID) NullUUID {
	return NullUUID{Valid: len(uuid) > 0, UUID: uuid}
}
