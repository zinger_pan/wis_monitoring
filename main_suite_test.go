package main_test

import (
	"context"
	"math/rand"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	main "monitoring-for-wis"
	dcclient "monitoring-for-wis/client/dbcore"
	gnclient "monitoring-for-wis/client/geonetwork"
	"monitoring-for-wis/config"
	"monitoring-for-wis/gpr"
	"monitoring-for-wis/keeper"
	dcmock "monitoring-for-wis/mock/dbcore"
	gnmock "monitoring-for-wis/mock/geonetwork"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/service"
)

const httpClientTimeout = 300 // ms, increase this value in slow environment

var (
	s        *service.Service
	g        *gpr.GPR
	gnMock   *gnmock.MockServer
	dcMock   *dcmock.MockServer
	storage  keeper.Keeper
	dcClient *dcclient.DCClient
	gnClient *gnclient.GNClient
)

func waitNextSecond() {
	nowSecond := func() int { return time.Now().Second() }
	s := nowSecond()
	EventuallyWithOffset(1, nowSecond, "1100ms").Should(BeNumerically(">", s))
}

var _ = Describe("Suite hooks", func() {
	var server *httptest.Server
	BeforeSuite(func() {
		By("initializing mocks", func() {
			gnMock = gnmock.New()
			gnMock.SetBasicAuth("admin", "admin")
			dcMock = dcmock.New()
			dcMock.SetBasicAuth("admin", "admin")
		})

		viper.Set("logLevel", logrus.DebugLevel.String())

		By("initializing config", func() {
			viper.SetConfigType("yml")
			viper.Set("httpClient.timeout", httpClientTimeout)
			viper.AutomaticEnv()
			replacer := strings.NewReplacer(".", "_")
			viper.SetEnvKeyReplacer(replacer)
			Expect(config.ViperLoadIntoCurrent()).To(Succeed())
			Expect(service.New(viper.GetViper())).To(Succeed())
		})

		By("preparing keeper, resolver and service", func() {
			var err error
			By("cleanup for postgres tests", func() {
				storage = keeper.NewObjectsService(keeper.NewTestsObjectsServiceParams())
				Expect(err).NotTo(HaveOccurred())
				Expect(storage).NotTo(BeNil())
				Expect(storage.DeleteAll(context.Background())).To(Succeed())
				Expect(storage.Close()).To(Succeed())
			})

			serviceResolver := resolver.NewConfigResolver(viper.GetViper())
			s = service.Get()
			gnClient = gnclient.New(s.HTTPClient, serviceResolver)
			dcClient = dcclient.New(s.HTTPClient, serviceResolver)
			s.SetGNClient(gnClient)
			s.SetDCClient(dcClient)

			storage = keeper.NewObjectsService(keeper.NewTestsObjectsServiceParams())
			s.SetKeeper(storage)

			Expect(config.SoftSet(func(c *config.Entire) {
				c.Services = map[string]config.Service{
					resolver.ServiceGeoNetwork: {Host: gnMock.Server.URL, Path: "/geonetwork"},
					resolver.ServiceDbCore:     {Host: dcMock.Server.URL, Path: "/api/v2"},
				}
				c.Auth = map[string]string{
					resolver.ServiceGeoNetwork: "admin:admin",
					resolver.ServiceDbCore:     "admin:admin",
				}
				c.Tickers.DbCoreMessagesPollInterval = config.TickersDbCoreNewMessages
			})).To(Succeed())

		})

		By("preparing HTTP server and test client", func() {
			httpServer := s.HTTPServer
			main.RegisterHTTPAPIHandlers(httpServer)
			server = httptest.NewServer(httpServer)

			g = gpr.New(server)
			g.SetLogPerformRequest(true)

			// set server port if needed
			URL, err := url.Parse(server.URL)
			Expect(err).NotTo(HaveOccurred())
			port, err := strconv.Atoi(URL.Port())
			Expect(err).NotTo(HaveOccurred())
			viper.Set(config.Port, uint(port))
		})
	})

	AfterSuite(func() {
		gnMock.Close()
		dcMock.Close()
		server.Close()
	})
})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "Main Suite")
}
