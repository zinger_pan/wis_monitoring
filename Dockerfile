FROM golang:1.16.4 as builder
WORKDIR '/app'
COPY . .
RUN go build -v -o app

FROM ubuntu as runner
COPY --from=builder /app/app /app
COPY --from=builder /app/config.yml /config.yml
ENV PORT $PORT
CMD ["sh", "-c", "/app --port=$PORT"]