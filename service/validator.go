package service

import (
	"bytes"
	"fmt"
	"reflect"
	"regexp"

	"gorm.io/datatypes"

	"github.com/go-playground/validator/v10"
)

// Validation implements echo.Validator interface
type Validation struct{ Validator *validator.Validate }

// NewValidation returns new validation object
func NewValidation() (*Validation, error) {
	v := &Validation{Validator: validator.New()}
	// registering custom validation funcs
	for i, el := range []struct {
		tag string
		fn  func(level validator.FieldLevel) bool
	}{
		{tag: "be_regexp", fn: BeRegexp},
		{tag: "be_json_object", fn: BeJSONObject},
	} {
		if err := v.Validator.RegisterValidation(el.tag, el.fn); err != nil {
			return nil, fmt.Errorf("NewValidation() -> v.Validator.RegisterValidation, i=%d, tag=%s", i, el.tag)
		}
	}
	return v, nil
}

// BeRegexp is a validator of the field being a regexp
func BeRegexp(fl validator.FieldLevel) bool {
	_, err := regexp.Compile(fl.Field().String())
	return err == nil
}

// BeJSONObject is a validator of the field being a JSON object (for type datatypes.JSON)
func BeJSONObject(fl validator.FieldLevel) bool {
	trimmed := bytes.TrimSpace(fl.Field().Interface().(datatypes.JSON))
	return bytes.HasPrefix(trimmed, []byte("{")) && bytes.HasSuffix(trimmed, []byte("}"))
}

// Validate makes Validation to implement echo.Validator interface
func (v Validation) Validate(i interface{}) error {
	indirect := reflect.Indirect(reflect.ValueOf(i))
	if indirect.IsZero() {
		return nil
	}
	if reflect.TypeOf(indirect).Kind() == reflect.Struct {
		return v.Validator.Struct(i)
	}
	return nil
}
