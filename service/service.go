package service

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"monitoring-for-wis/client/dbcore"
	"monitoring-for-wis/client/geonetwork"
	"monitoring-for-wis/client/metadata"
	"monitoring-for-wis/config"
	"monitoring-for-wis/keeper"
	"monitoring-for-wis/monitoring"
	"monitoring-for-wis/resolver"
)

// Service represents service singleton. Contains global internal service infrastructure
type Service struct {
	Logger        *logrus.Logger
	Configuration *viper.Viper
	HTTPServer    *echo.Echo

	GNClient   *geonetwork.GNClient   // intermediate layer
	MDClient   *metadata.MDClient     // intermediate layer
	DCClient   *dbcore.DCClient       // intermediate layer
	Monitoring *monitoring.Monitoring // intermediate layer
	Resolver   resolver.Resolver
	HTTPClient *http.Client
	Keeper     keeper.Keeper
	Validation *validator.Validate
}

var singleton *Service

// Get provides access to a service components
func Get() *Service { return singleton }

// newService creates new service object
func newService(configuration *viper.Viper) error {
	validation, err := NewValidation()
	if err != nil {
		return err
	}
	singleton = &Service{
		Logger:        logrus.StandardLogger(),
		Configuration: configuration,
		HTTPServer:    echo.New(),
		HTTPClient: &http.Client{
			Timeout: config.Clone().HTTPClient.TimeoutDuration(),
		},
		Validation: validation.Validator,
	}
	singleton.HTTPServer.Validator = validation
	return nil
}

// SetGNClient into service
func (s *Service) SetGNClient(client *geonetwork.GNClient) { s.GNClient = client }

// SetMDTClient into service
func (s *Service) SetMDClient(client *metadata.MDClient) { s.MDClient = client }

// SetDCClient into service
func (s *Service) SetDCClient(client *dbcore.DCClient) { s.DCClient = client }

// SetMonitoring into service
func (s *Service) SetMonitoring(monitoring *monitoring.Monitoring) { s.Monitoring = monitoring }

// SetKeeper into service
func (s *Service) SetKeeper(keeper keeper.Keeper) { s.Keeper = keeper }

// New creates new service object
func New(conf *viper.Viper) error { return newService(conf) }
