package resolver

import (
	"errors"
	"net/url"
	"sync"

	"github.com/spf13/viper"
)

var resolverLock sync.Mutex // todo should get rid of this: seems fixing data race in Viper

// ErrServiceIsNotRegistered is raised when no such service registered
var ErrServiceIsNotRegistered = errors.New("service is not registered")

// ConfigResolver implements service resolving from the given configuration
type ConfigResolver struct{ config *viper.Viper }

// NewConfigResolver creates and returns new configuration-based service resolver
func NewConfigResolver(config *viper.Viper) ConfigResolver {
	return ConfigResolver{config: config}
}

// Resolve the service by it's name
func (c ConfigResolver) Resolve(name string) (Service, error) {
	var service map[string]Service
	if err := func() error {
		resolverLock.Lock()
		defer resolverLock.Unlock()
		return c.config.UnmarshalKey("services", &service)
	}(); err != nil {
		return Service{}, err
	}

	confService, ok := service[name]
	if !ok {
		return Service{}, ErrServiceIsNotRegistered
	}

	uri, err := url.ParseRequestURI(confService.Host)
	if err != nil {
		return Service{}, err
	}
	confService.Host = uri.Host
	confService.Scheme = uri.Scheme

	return confService, nil
}
