package resolver_test

import (
	"math/rand"
	"testing"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"monitoring-for-wis/config"
	"monitoring-for-wis/resolver"
)

var _ = Describe("tests for the service resolver", func() {
	BeforeSuite(func() {
		logrus.SetLevel(logrus.DebugLevel)
	})

	var serviceResolver resolver.Resolver

	BeforeEach(func() {
		viper.SetConfigType("yml")
		Expect(config.Set(config.Entire{
			Services: map[string]config.Service{
				resolver.ServiceGeoNetwork: {
					Host: "http://127.0.0.1:8200",
					Path: "/geonetwork/smth",
				},
				resolver.ServiceDbCore: {
					Host: "http://127.0.0.1:8201",
					Path: "/api/v2",
				},
			},
		}, true, false)).To(Succeed())
		serviceResolver = resolver.NewConfigResolver(viper.GetViper())
		Expect(serviceResolver).NotTo(BeNil())
	})

	It("checks configuration-based resolver", func() {
		By("one service", func() {
			srv, err := serviceResolver.Resolve(resolver.ServiceGeoNetwork)
			Expect(err).NotTo(HaveOccurred())
			Expect(srv).To(Equal(resolver.Service{
				Host:   "127.0.0.1:8200",
				Path:   "/geonetwork/smth",
				Scheme: "http",
			}))
		})
		By("another one", func() {
			srv, err := serviceResolver.Resolve(resolver.ServiceDbCore)
			Expect(err).NotTo(HaveOccurred())
			Expect(srv).To(Equal(resolver.Service{
				Host:   "127.0.0.1:8201",
				Path:   "/api/v2",
				Scheme: "http",
			}))
		})
	})
})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "Resolver Suite")
}
