package resolver

// Resolver abstracts service resolver
type Resolver interface {
	Resolve(name string) (Service, error)
}
