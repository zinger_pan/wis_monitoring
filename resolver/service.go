package resolver

// Service is a service definition
type Service struct {
	Host   string `json:"host"`
	Path   string `json:"path"`
	Scheme string `json:"scheme"`
}
