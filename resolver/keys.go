package resolver

// service key constants
const (
	ServiceGeoNetwork    = "geonetwork"
	ServiceDbCore        = "dbcore"
	ServiceObjectStorage = "objects"
	ServiceMetadata      = "metadata"
)
