package geonetwork

const (
	APIForbiddenStub = `
{
  "meta": {
    "code": 403,
    "rid": "9f8523e6-c761-11eb-8291-4439c46d3b6b",
    "time": "0.232"
  }
}`
	APISystemResponseStub = `
{
  "meta": {
    "code": 200,
    "rid": "9f8523e6-c761-11eb-8291-4439c46d3b6b",
    "time": "0.232"
  },
  "response": {
    "commit": "15d35d860",
    "date": "2021-05-19T13:33:42",
    "hid": "009-XXXX-0000",
    "name": "dbcore",
    "server_time": {
      "loc": 1623061566,
      "meteoRange": 900,
      "timeShift": 180,
      "utc": 1623050766
    },
    "services": {
      "cccc": [
        "RUMA",
        "RAMA"
      ],
      "orgs": [
        1,
        2,
        3,
        4
      ],
      "regions": [
        "RU-AAA",
        "RU-BBB"
      ]
    },
    "ver": "1.8.81"
  }
}`
	APIStorageListResponseStub = `
{
  "meta":{
    "code":200,
    "rid":"9f6d3f22-d28e-11eb-8ce5-000c2972e409",
    "time":"146.463"
  },
  "response":{
    "jrecs":{
      "count":1,
      "items":[
        {
          "attachments":[
            
          ],
          "attrs":{
            "cccc":"RUWS",
            "format":"WMO",
            "from":"192.168.3.200:38765",
            "shortheader":"VURS01 RUWS 211245",
            "sys.queue":"8fc18e1f-b27c-11e7-bf03-e069955b7462, 4e421783-f272-11ea-bbec-000c2972e409, 9a23cfd7-f283-11ea-a899-000c2972e409",
            "textpos":21
          },
          "bt":"message",
          "file_link":{
            
          },
          "first":"VURS01 RUWS 211245   FILE 1310 2021_06_21_12_45utc.lf   0 2021 6 21 12 43 59 1194240 43.6335 32.6161 -55 0 8 7 53.20 3.90 0.60 0.60 7.8 39.0 -16.9 0 1 0 1  0 2021 6 21 12 44 6 455982080 59.7811 30.5566 -56 0 66 141.30 16.30 2.10 1.00 4.2 30.2 -0.0 0 1...",
          "hash":"b9898b11d92a77b0b5552341784233fd",
          "history":[
            
          ],
          "jrid":"8148b47c-d28e-11eb-8ce5-000c2972e409",
          "key":"vurs01ruws",
          "name":"VURS01 RUWS 211245",
          "priority":1,
          "size":1366,
          "time":1624279506,
          "who":"f03eeee6-e083-11ea-8ec9-000c2972e409"
        }
      ]
    }
  }
}`

	APIStorageListEmptyResponseStub = `
{
  "meta":{
    "code":200,
    "rid":"9f6d3f22-d28e-11eb-8ce5-000c2972e409",
    "time":"146.463"
  },
  "response":{
    "jrecs":{
      "count":0,
      "items":[]
    }
  }
}`
)
