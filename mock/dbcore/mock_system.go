package geonetwork

import (
	"encoding/json"
	"net/http"
)

func (m *MockServer) apiSystem(p imitatorParams) error {
	if login, password, ok := p.r.BasicAuth(); !ok || login != m.login || password != m.password {
		return m.writeResponse(p.w, http.StatusForbidden, json.RawMessage(APIForbiddenStub))
	}

	if len(p.subMatches) != 1 {
		panic("uriSubMatches should have len of 1")
	}
	return m.writeResponse(p.w, http.StatusOK, json.RawMessage(APISystemResponseStub))
}
