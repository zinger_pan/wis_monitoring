package geonetwork

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sync"
)

// MockServer for testing GeoNetwork-related API
type MockServer struct {
	*httptest.Server
	sync.Mutex

	unhangC            chan struct{}
	unhangClosed       bool
	hung               bool // true if mock is hung
	hungRequestCounter int
	unhangAfterCount   int // unhang after request counter exceeds this value

	login, password string
}

type imitatorParams struct {
	w          http.ResponseWriter
	r          *http.Request
	subMatches []string
	urlValues  url.Values
}

// SetBasicAuth credentials into a mock server
func (m *MockServer) SetBasicAuth(login, password string) { m.login, m.password = login, password }

func (m *MockServer) writeResponse(w http.ResponseWriter, code int, data interface{}) (err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	var b []byte
	if b, err = json.Marshal(data); err != nil {
		return err
	}
	_, err = fmt.Fprint(w, string(b))
	return err
}

// New returns a pointer to new GeoNetwork mock server
func New() *MockServer {
	s := &MockServer{}
	s.Server = httptest.NewServer(http.HandlerFunc(s.routing))
	return s
}

// Hang orders mock to hang answering requests
func (m *MockServer) Hang() {
	m.Lock()
	if m.unhangC == nil {
		m.unhangC, m.unhangClosed = make(chan struct{}), false
	}
	m.hung = true
	m.Unlock()
}

// Close the mock server
func (m *MockServer) Close() {
	m.Unhang()
	m.Server.Close()
}

// Unhang orders mock to stop hanging answering requests (already hung requests still hung)
func (m *MockServer) Unhang() {
	m.Lock()
	m.hung = false
	if m.unhangC != nil && !m.unhangClosed {
		close(m.unhangC)
		m.unhangClosed = true
	}
	m.Unlock()
}

// Hung returns true if mock is hung, otherwise returns false
func (m *MockServer) Hung() bool {
	m.Lock()
	defer m.Unlock()
	return m.hung
}

// ProceedAfterCount orders mock to proceed answering requests after request counter exceeds the given value c
func (m *MockServer) ProceedAfterCount(c int) {
	m.Lock()
	m.unhangAfterCount = c
	m.Unlock()
}

// Reset the mock server
func (m *MockServer) Reset() {
	m.Unhang()
	m.hungRequestCounter = 0
}
