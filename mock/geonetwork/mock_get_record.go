package geonetwork

import (
	"encoding/json"
	"net/http"
)

func (m *MockServer) apiRecordGet(p imitatorParams) error {
	if login, password, ok := p.r.BasicAuth(); !ok || login != m.login || password != m.password {
		return m.writeResponse(p.w, mimeTypeJSON, http.StatusForbidden, json.RawMessage(APIForbiddenStub))
	}
	if len(p.subMatches) != 2 {
		panic("uriSubMatches should have len of 2")
	}
	return m.writeResponse(p.w, mimeTypeXML, http.StatusOK, APIRecordGetResponseStub)
}
