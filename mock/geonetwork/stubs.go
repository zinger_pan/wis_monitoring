package geonetwork

const (
	APIForbiddenStub = `
{
  "message":"Access denied",
  "code":"forbidden",
   "description":"Access is denied. To access, try again with a user containing more priviledges."
}`
	APIRecordGetResponseStub = `
<?xml version="1.0" encoding="UTF-8"?>
<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:gts="http:
//www.isotc211.org/2005/gts" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://www.ngdc.noaa.gov/metadat
a/published/xsd/schema/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://www.ngdc.noaa.gov/metadata/published/xsd/schema/gmx/gmx.xsd">
  <gmd:fileIdentifier xmlns:gml="http://www.opengis.net/gml">
    <gco:CharacterString>urn:x-wmo:md:int.wmo.wis::ISII01EGRR</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gmd:LanguageCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/gmxCodelists.xml#LanguageCode" codeListValue="eng">eng</gmd:LanguageCode>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/gmxCodelists.xml#MD_CharacterSetCode" codeListValue="utf8">utf8</gmd:MD_CharacterSetCode>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="series" />
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>Series of WMO GTS bulletins</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty id="publisher">
      <gmd:individualName>
        <gco:CharacterString>Chris Little</gco:CharacterString>
      </gmd:individualName>
      <gmd:organisationName>
        <gco:CharacterString>UK Met Office</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice>
                <gco:CharacterString>+44 1392 886278</gco:CharacterString>
              </gmd:voice>
              <gmd:facsimile>
                <gco:CharacterString>+44 1392 885681</gco:CharacterString>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint>
                <gco:CharacterString>FitzRoy Road</gco:CharacterString>
              </gmd:deliveryPoint>
              <gmd:city>
                <gco:CharacterString>Exeter</gco:CharacterString>
              </gmd:city>
              <gmd:postalCode>
                <gco:CharacterString>EX20 3PB</gco:CharacterString>
              </gmd:postalCode>
              <gmd:country>
                <gco:CharacterString>United Kingdom</gco:CharacterString>
              </gmd:country>
              <gmd:electronicMailAddress>
                <gco:CharacterString>chris.little@metoffice.gov.uk</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
          <gmd:onlineResource>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>http://www.wmo.int/pages/prog/www/WIS/wiswiki/tiki-index.php?page=metadatagen_fr</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
              <gmd:name gco:nilReason="missing">
                <gco:CharacterString />
              </gmd:name>
              <gmd:description>
                <gco:CharacterString>Go there for a general overview and further details on reviewing GTS metadata records</gco:CharacterString>
              </gmd:description>
              <gmd:function>
                <gmd:CI_OnLineFunctionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_OnLineFunctionCode" codeListValue="information" />
              </gmd:function>
            </gmd:CI_OnlineResource>
          </gmd:onlineResource>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="author" />
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime xmlns:gml="http://www.opengis.net/gml">2014-11-11T15:21:58</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>WMO Core Profile of ISO 19115</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.2 (WMO Core), 2003/Cor.1:2006 (ISO 19115), 2007 (ISO/TS 19139)</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:metadataExtensionInfo gco:nilReason="missing" />
  <gmd:identificationInfo>
    <gmd:MD_DataIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>ISII01 collection of SYNOP reports available from EGRR (EXETER) at 03, 09, 15 and 21 UTC</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:Date>2014-11-11</gco:Date>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication" />
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
          <gmd:identifier>
            <gmd:MD_Identifier>
              <gmd:authority>
                <gmd:CI_Citation>
                  <gmd:title>
                    <gco:CharacterString>WMO Information System (WIS)</gco:CharacterString>
                  </gmd:title>
                  <gmd:date gco:nilReason="inapplicable" />
                </gmd:CI_Citation>
              </gmd:authority>
              <gmd:code>
                <gco:CharacterString>urn:x-wmo:bulletin:int.wmo.wis::ISII01EGRR</gco:CharacterString>
              </gmd:code>
            </gmd:MD_Identifier>
          </gmd:identifier>
          <gmd:presentationForm>
            <gmd:CI_PresentationFormCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_PresentationFormCode" codeListValue="documentDigital" />
          </gmd:presentationForm>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>---- The bulletin collects SYNOP reports:
FM 94(SYNOP, Report of surface observation from a fixed land station).


(Refer to WMO No.306 - Manual on Codes for the definition of WMO international codes)

---- The ISII01 TTAAii Data Designators decode (2) as:
T1T2 (IS): Surface data.
A1 (I): Intermediate synoptic hour.
A2 (I): South Atlantic area.

(2: Refer to WMO No.386 - Manual on the GTS - Attachment II.5)

---- The bulletin collects reports from stations:
St. helena is., Wide awake field (ascension is.), Pebble island, Weddell island, Mount pleasant airport, Sea lion island, Bird island, south georgia, Grytviken and  south georgia

---- Please review:
Bulletin constraints (gmd:resourceConstraints) should reflect the GTS Category (WMO_DataLicenseCode) and GTS Priority (WMO_GTSProductCategoryCode) of the bulletin.
Bulletin Originator (gmd:pointOfContact).
Bulletin Distributor (gmd:distributorContact and gmd:name in all instances of gmd:MD_DigitalTransferOptions).
Online distribution details for the bulletin (all instances of gmd:MD_DigitalTransferOptions).
MANDATED: Insert at least one thematic keyword from the WMO_CodeList dictionary (gmd:MD_Keywords/@id="WMOCodeListKeywords").</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty id="NC-GB-1">
          <gmd:organisationName>
            <gco:CharacterString>NMC UNITED KINGDOM - Met Office</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice>
                    <gco:CharacterString>T+44 1392885680</gco:CharacterString>
                  </gmd:voice>
                  <gmd:facsimile>
                    <gco:CharacterString>F+44 1392885681</gco:CharacterString>
                  </gmd:facsimile>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:deliveryPoint>
                    <gco:CharacterString>FitzRoy Road</gco:CharacterString>
                  </gmd:deliveryPoint>
                  <gmd:city>
                    <gco:CharacterString>EXETER</gco:CharacterString>
                  </gmd:city>
                  <gmd:postalCode>
                    <gco:CharacterString>EX1 3PB</gco:CharacterString>
                  </gmd:postalCode>
                  <gmd:country>
                    <gco:CharacterString>UNITED KINGDOM</gco:CharacterString>
                  </gmd:country>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>enquiries@metoffice.gov.uk</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
              <gmd:onlineResource>
                <gmd:CI_OnlineResource>
                  <gmd:linkage>
                    <gmd:URL>http://www.metoffice.gov.uk/</gmd:URL>
                  </gmd:linkage>
                </gmd:CI_OnlineResource>
              </gmd:onlineResource>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="pointOfContact" />
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:resourceMaintenance>
        <gmd:MD_MaintenanceInformation>
          <gmd:maintenanceAndUpdateFrequency gco:nilReason="inapplicable" />
          <gmd:userDefinedMaintenanceFrequency>
            <gts:TM_PeriodDuration>P0Y0M0DT6H0M0S</gts:TM_PeriodDuration>
          </gmd:userDefinedMaintenanceFrequency>
          <gmd:updateScope>
            <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="series" />
          </gmd:updateScope>
          <gmd:updateScopeDescription>
            <gmd:MD_ScopeDescription>
              <gmd:dataset>
                <gco:CharacterString>Instances of bulletin ISII01EGRR are available every 6 hours starting at 3 UTC</gco:CharacterString>
              </gmd:dataset>
            </gmd:MD_ScopeDescription>
          </gmd:updateScopeDescription>
        </gmd:MD_MaintenanceInformation>
      </gmd:resourceMaintenance>
      <gmd:resourceFormat>
        <gmd:MD_Format>
          <gmd:name>
            <gco:CharacterString>BUFR SYNOP</gco:CharacterString>
          </gmd:name>
          <gmd:version>
            <gco:CharacterString>94 X-III EXT.</gco:CharacterString>
          </gmd:version>
          <gmd:specification>
            <gco:CharacterString>http://www.wmo.int/pages/prog/www/WMOCodes.html</gco:CharacterString>
          </gmd:specification>
        </gmd:MD_Format>
      </gmd:resourceFormat>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords id="WMOCodeListKeywords">
          <gmd:keyword gco:nilReason="unknown" />
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme" />
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation id="WMOCodelist">
              <gmd:title>
                <gco:CharacterString>WMO_CodeList dictionary [http://wis.wmo.int/2010/metadata/version_1-2/WMOCodelists.xml#WMO_CategoryCode]</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2010-11-13</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="revision" />
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
              <gmd:identifier>
                <gmd:MD_Identifier>
                  <gmd:authority>
                    <gmd:CI_Citation>
                      <gmd:title>
                        <gco:CharacterString>Codelists for description of metadata datasets compliant with the WMO Core Metadata Profile version 1.2 [http://wis.wmo.int/2010/metadata/version_1-2/WMOCodelists.xml
]</gco:CharacterString>
                      </gmd:title>
                      <gmd:date>
                        <gmd:CI_Date>
                          <gmd:date>
                            <gco:Date>2010-11-13</gco:Date>
                          </gmd:date>
                          <gmd:dateType>
                            <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="revision" />
                          </gmd:dateType>
                        </gmd:CI_Date>
                      </gmd:date>
                    </gmd:CI_Citation>
                  </gmd:authority>
                  <gmd:code>
                    <gco:CharacterString>WMO_CategoryCode</gco:CharacterString>
                  </gmd:code>
                </gmd:MD_Identifier>
              </gmd:identifier>
              <gmd:citedResponsibleParty>
                <gmd:CI_ResponsibleParty id="CH-WMOSec">
                  <gmd:organisationName>
                    <gco:CharacterString>WMO Secretariat</gco:CharacterString>
                  </gmd:organisationName>
                  <gmd:role>
                    <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="publisher" />
                  </gmd:role>
                </gmd:CI_ResponsibleParty>
              </gmd:citedResponsibleParty>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords id="GEMETKeywords">
          <gmd:keyword>
            <gco:CharacterString>atmospheric conditions</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme" />
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation id="GEMET">
              <gmd:title>
                <gco:CharacterString>GEMET - INSPIRE themes, version 1.0</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2008-06-01</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication" />
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
              <gmd:citedResponsibleParty>
                <gmd:CI_ResponsibleParty>
                  <gmd:organisationName>
                    <gco:CharacterString>GEneral Multilingual Environmental Thesaurus from the European Agency for Environment</gco:CharacterString>
                  </gmd:organisationName>
                  <gmd:contactInfo>
                    <gmd:CI_Contact>
                      <gmd:onlineResource>
                        <gmd:CI_OnlineResource>
                          <gmd:linkage>
                            <gmd:URL>http://www.eionet.europa.eu/gemet</gmd:URL>
                          </gmd:linkage>
                        </gmd:CI_OnlineResource>
                      </gmd:onlineResource>
                    </gmd:CI_Contact>
                  </gmd:contactInfo>
                  <gmd:role>
                    <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="publisher" />
                  </gmd:role>
                </gmd:CI_ResponsibleParty>
              </gmd:citedResponsibleParty>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>61902</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="" />
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords id="WMONo9VolAKeywords">
          <gmd:keyword>
            <gco:CharacterString>61901</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>FHAW</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>88878</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>88883</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>88889</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>88897</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>88900</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>88903</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place" />
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation id="WMONo9VolA">
              <gmd:title>
                <gco:CharacterString>WMO Publication No. 9, Volume A, Observing Stations and WMO Catalogue of Radiosondes [http://www.wmo.int/pages/prog/www/ois/volume-a/vola-home.htm]</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2010-10-19</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="revision" />
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
              <gmd:citedResponsibleParty>
                <gmd:CI_ResponsibleParty>
                  <gmd:organisationName>
                    <gco:CharacterString>WMO Secretariat</gco:CharacterString>
                  </gmd:organisationName>
                  <gmd:role>
                    <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="publisher" />
                  </gmd:role>
                </gmd:CI_ResponsibleParty>
              </gmd:citedResponsibleParty>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>88870</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="" />
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>88881</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="" />
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>88894</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="" />
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation>
            <gco:CharacterString>WMO Additional</gco:CharacterString>
          </gmd:useLimitation>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation>
            <gco:CharacterString>GTS Priority 2</gco:CharacterString>
          </gmd:useLimitation>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:spatialRepresentationType>
        <gmd:MD_SpatialRepresentationTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="vector" />
      </gmd:spatialRepresentationType>
      <gmd:language>
        <gmd:LanguageCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#LanguageCode" codeListValue="eng" />
      </gmd:language>
      <gmd:topicCategory>
        <gmd:MD_TopicCategoryCode>climatologyMeteorologyAtmosphere</gmd:MD_TopicCategoryCode>
      </gmd:topicCategory>
      <gmd:extent>
        <gmd:EX_Extent id="boundingExtent">
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-90</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>0</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>-90</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>0</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
          <gmd:temporalElement>
            <gmd:EX_TemporalExtent id="boundingTemporalExtent">
              <gmd:extent>
                <gml:TimePeriod gml:id="boundingTemporalExtentTP">
                  <gml:beginPosition>2014-11-11</gml:beginPosition>
                  <gml:endPosition indeterminatePosition="now" />
                </gml:TimePeriod>
              </gmd:extent>
            </gmd:EX_TemporalExtent>
          </gmd:temporalElement>
        </gmd:EX_Extent>
      </gmd:extent>
    </gmd:MD_DataIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:distributionFormat>
        <gmd:MD_Format>
          <gmd:name>
            <gco:CharacterString>BUFR SYNOP</gco:CharacterString>
          </gmd:name>
          <gmd:version>
            <gco:CharacterString>94 X-III EXT.</gco:CharacterString>
          </gmd:version>
          <gmd:specification>
            <gco:CharacterString>http://www.wmo.int/pages/prog/www/WMOCodes.html</gco:CharacterString>
          </gmd:specification>
        </gmd:MD_Format>
      </gmd:distributionFormat>
      <gmd:distributor>
        <gmd:MD_Distributor>
          <gmd:distributorContact>
            <gmd:CI_ResponsibleParty id="DCPC-GB-5">
              <gmd:individualName>
                <gco:CharacterString>Chris LITTLE</gco:CharacterString>
              </gmd:individualName>
              <gmd:organisationName>
                <gco:CharacterString>RTH Exeter - Met Office</gco:CharacterString>
              </gmd:organisationName>
              <gmd:contactInfo>
                <gmd:CI_Contact>
                  <gmd:address>
                    <gmd:CI_Address>
                      <gmd:deliveryPoint>
                        <gco:CharacterString>FitzRoy Road</gco:CharacterString>
                      </gmd:deliveryPoint>
                      <gmd:city>
                        <gco:CharacterString>EXETER</gco:CharacterString>
                      </gmd:city>
                      <gmd:postalCode>
                        <gco:CharacterString>EX1 3PB</gco:CharacterString>
                      </gmd:postalCode>
                      <gmd:country>
                        <gco:CharacterString>UNITED KINGDOM</gco:CharacterString>
                      </gmd:country>
                      <gmd:electronicMailAddress>
                        <gco:CharacterString>chris.little@metoffice.gov.uk</gco:CharacterString>
                      </gmd:electronicMailAddress>
                    </gmd:CI_Address>
                  </gmd:address>
                </gmd:CI_Contact>
              </gmd:contactInfo>
              <gmd:role>
                <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="pointOfContact" />
              </gmd:role>
            </gmd:CI_ResponsibleParty>
          </gmd:distributorContact>
          <gmd:distributorTransferOptions>
            <gmd:MD_DigitalTransferOptions>
              <gmd:onLine>
                <gmd:CI_OnlineResource>
                  <gmd:linkage>
                    <gmd:URL>UPDATE: http://PROVIDER_HOSTNAME/PROVIDER_PATH/urn%3Ax-wmo%3Aint.wmo.wis%3Amd%3A%3ASIST21EGRR</gmd:URL>
                  </gmd:linkage>
                  <gmd:protocol>
                    <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                  </gmd:protocol>
                  <gmd:name>
                    <gco:CharacterString>RTH Exeter</gco:CharacterString>
                  </gmd:name>
                  <gmd:description>
                    <gco:CharacterString>ISII01 collection of SYNOP reports available from EGRR (EXETER) at 03, 09, 15 and 21 UTC at RTH Exeter</gco:CharacterString>
                  </gmd:description>
                  <gmd:function>
                    <gmd:CI_OnLineFunctionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_OnLineFunctionCode" codeListValue="download" />
                  </gmd:function>
                </gmd:CI_OnlineResource>
              </gmd:onLine>
            </gmd:MD_DigitalTransferOptions>
          </gmd:distributorTransferOptions>
        </gmd:MD_Distributor>
      </gmd:distributor>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>UPDATE: http://PROVIDER_HOSTNAME/PROVIDER_PATH/urn%3Ax-wmo%3Aint.wmo.wis%3Amd%3A%3ASIST21EGRR</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
              <gmd:name>
                <gco:CharacterString>RTH Exeter</gco:CharacterString>
              </gmd:name>
              <gmd:description>
                <gco:CharacterString>ISII01 collection of SYNOP reports available from EGRR (EXETER) at 03, 09, 15 and 21 UTC at RTH Exeter</gco:CharacterString>
              </gmd:description>
              <gmd:function>
                <gmd:CI_OnLineFunctionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_OnLineFunctionCode" codeListValue="download" />
              </gmd:function>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="series" />
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:report>
        <gmd:DQ_DomainConsistency>
          <gmd:measureIdentification>
            <gmd:RS_Identifier>
              <gmd:code>
                <gco:CharacterString>Conformity_001</gco:CharacterString>
              </gmd:code>
              <gmd:codeSpace>
                <gco:CharacterString>INSPIRE</gco:CharacterString>
              </gmd:codeSpace>
            </gmd:RS_Identifier>
          </gmd:measureIdentification>
          <gmd:result>
            <gmd:DQ_ConformanceResult>
              <gmd:specification>
                <gmd:CI_Citation>
                  <gmd:title>
                    <gco:CharacterString>INSPIRE Implementing Rules</gco:CharacterString>
                  </gmd:title>
                  <gmd:date>
                    <gmd:CI_Date>
                      <gmd:date>
                        <gco:Date>2011-05-15</gco:Date>
                      </gmd:date>
                      <gmd:dateType>
                        <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication" />
                      </gmd:dateType>
                    </gmd:CI_Date>
                  </gmd:date>
                </gmd:CI_Citation>
              </gmd:specification>
              <gmd:explanation gco:nilReason="missing">
                <gco:CharacterString />
              </gmd:explanation>
              <gmd:pass>
                <gco:Boolean>true</gco:Boolean>
              </gmd:pass>
            </gmd:DQ_ConformanceResult>
          </gmd:result>
        </gmd:DQ_DomainConsistency>
      </gmd:report>
      <gmd:lineage>
        <gmd:LI_Lineage>
          <gmd:statement>
            <gco:CharacterString>High data quality controlled according to the procedures of the GTS</gco:CharacterString>
          </gmd:statement>
        </gmd:LI_Lineage>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
  <gmd:metadataConstraints>
    <gmd:MD_Constraints>
      <gmd:useLimitation gco:nilReason="missing">
        <gco:CharacterString />
      </gmd:useLimitation>
    </gmd:MD_Constraints>
  </gmd:metadataConstraints>
</gmd:MD_Metadata>`
)
