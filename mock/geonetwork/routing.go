package geonetwork

import (
	"fmt"
	"net/http"
	"net/url"
	"regexp"

	"github.com/sirupsen/logrus"
)

type imitatorFunc func(imitatorParams) error

// mock API urls
var (
	recordGetAddr = regexp.MustCompile(`(?i)/geonetwork/srv/api/0.1/records/([\w:\.-])+`)

	logger = logrus.WithField("mock", "dbcore")
)

func (m *MockServer) routing(w http.ResponseWriter, r *http.Request) {
	m.Lock()
	if m.unhangAfterCount > 0 && m.hungRequestCounter >= m.unhangAfterCount {
		m.hung, m.hungRequestCounter = false, 0
	}
	m.Unlock()
	if m.Hung() {
		m.Lock()
		m.hungRequestCounter++
		m.Unlock()
		fmt.Println("MOCK request:", r.RequestURI, "(HUNG)")
		<-m.unhangC
		return
	}

	m.Lock()
	defer m.Unlock()

	var (
		imFunc     imitatorFunc
		subMatches []string
	)
	switch {
	// order of cases matters because of regexp checks

	case recordGetAddr.MatchString(r.RequestURI) && r.Method == http.MethodGet:
		imFunc, subMatches = m.apiRecordGet, recordGetAddr.FindStringSubmatch(r.RequestURI)

	default:
		panic("MockServer unknown request: " + r.RequestURI)
	}

	var err error
	p := imitatorParams{w: w, r: r, subMatches: subMatches}
	if p.urlValues, err = url.ParseQuery(r.URL.RawQuery); err != nil {
		panic(err)
	}
	if err = imFunc(p); err != nil {
		logger.Warn(err)
	}
}
