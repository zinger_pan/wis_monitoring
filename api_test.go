package main_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"monitoring-for-wis/api/signal"

	"monitoring-for-wis/api/rule"

	"github.com/mtfelian/utils"
	ut "gorm.io/gorm/utils"

	"github.com/go-openapi/strfmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	uuid "github.com/satori/go.uuid"
	"monitoring-for-wis/keeper/models"
	dcmock "monitoring-for-wis/mock/dbcore"
	"monitoring-for-wis/monitoring"
	"monitoring-for-wis/response"
	"monitoring-for-wis/service"
)

var _ = Describe("Testing API", func() {
	var mon *monitoring.Monitoring
	BeforeEach(func() {
		// first we purge storage
		Expect(service.Get().Keeper.DeleteAll(context.Background())).To(Succeed())

		// next we create a new monitoring instance, so cache will be empty
		var err error
		mon, err = monitoring.New(dcClient, gnClient, mdClient, storage)
		Expect(err).NotTo(HaveOccurred())
		s.SetMonitoring(mon)
	})

	AfterEach(func() {
		mon.Stop()
		mon.WaitForScheduledTasks()
	})

	Describe("DbCore proxying", func() {
		Context("system request", func() {
			It("checks that it works", func() {
				var response json.RawMessage
				g.PerformSystemGetRequest(http.StatusOK, &response)
				Expect(response).To(MatchJSON(dcmock.APISystemResponseStub))
			})

			It("checks that it don't work because of a client layer error", func() {
				dcMock.Hang()
				defer dcMock.Unhang()

				var response response.Default
				g.PerformSystemGetRequest(http.StatusInternalServerError, &response)
				Expect(response.Code).NotTo(BeZero())
			})
		})
	})

	Describe("rules API", func() {
		var ID2, ID1, compID1, compID2, noID strfmt.UUID
		var urn1, urn2 string

		BeforeEach(func() {
			ID1 = strfmt.UUID(uuid.NewV4().String())
			ID2 = strfmt.UUID(uuid.NewV4().String())
			compID1 = strfmt.UUID(uuid.NewV4().String())
			compID2 = strfmt.UUID(uuid.NewV4().String())
			noID = strfmt.UUID(uuid.NewV4().String())
			urn1 = "urn:x-wmo:md:int.wmo.wis::IUSN53LFPW"
			urn2 = "urn:x-wmo:md:int.wmo.wis::IUSN54LFPW"

			By("creating rule 1", func() {
				req := fmt.Sprintf(`{"rulid":%q, "compid": %q, "urn":%q, "name": "some name", "data": {"schedule": "every 60 min", "regexp":"exp1"}}`, ID1, compID1, urn1)
				res := new(rule.AddResponse)
				g.PerformRuleCreateRequest(http.StatusCreated, []byte(req), &res)

			})
			By("creating rule 2", func() {
				req := fmt.Sprintf(`{"rulid":%q, "compid": %q, "urn":%q, "name": "some name2", "data": {"schedule": "every 30 min", "regexp":"exp2"}}`, ID2, compID2, urn2)
				res := new(rule.AddResponse)
				g.PerformRuleCreateRequest(http.StatusCreated, []byte(req), &res)
			})
		})

		It("checks listing all rules", func() {
			var res rule.ListResponse
			g.PerformRuleListRequest(http.StatusOK, nil, &res)
			Expect(res.Items).To(HaveLen(2))
			Expect(res.Items.URNs()).To(ConsistOf([]string{urn1, urn2}))
		})

		It("checks listing rules by search string", func() {
			res := new(rule.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("searchStr", "some name2")
			urlVals.Add("search", "name")
			g.PerformRuleListRequest(http.StatusOK, urlVals, &res)
			Expect(res.Items).To(HaveLen(1))
			Expect(res.Items[0].Name).To(Equal("some name2"))
			Expect(res.Items[0].URN).To(Equal(urn2))
		})

		It("checks listing rules by search string", func() {
			res := new(rule.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("searchStr", urn2)
			urlVals.Add("search", "urn")
			g.PerformRuleListRequest(http.StatusOK, urlVals, &res)
			Expect(res.Items).To(HaveLen(1))
			Expect(res.Items[0].Name).To(Equal("some name2"))
			Expect(res.Items[0].URN).To(Equal(urn2))
		})

		It("checks listing rules with given URN", func() {
			var res rule.ListResponse
			data := url.Values{"urn": {urn1}}
			g.PerformRuleListRequest(http.StatusOK, data, &res)
			Expect(res.Items).To(HaveLen(1))
			Expect(res.Items[0].URN).To(Equal(urn1))
		})

		It("checks listing rules from to all", func() {
			now := time.Now().Unix()
			res := new(rule.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("updatedfrom", ut.ToString(now-5))
			urlVals.Add("updatedto", ut.ToString(now+5))
			g.PerformRuleListPostRequest(http.StatusOK, urlVals, nil, &res)
			Expect(res.Items).To(HaveLen(2))
		})

		It("checks listing rules from to should be empty", func() {
			now := time.Now().Unix()
			res := new(rule.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("updatedfrom", ut.ToString(now-25))
			urlVals.Add("updatedto", ut.ToString(now-5))
			g.PerformRuleListPostRequest(http.StatusOK, urlVals, nil, &res)
			Expect(res.Items).To(HaveLen(0))
		})

		Context("get rule", func() {
			It("checks getting rule if exists", func() {
				now := time.Now().Unix()
				res := new(rule.GetResponse)
				g.PerformRuleGetRequest(http.StatusOK, ID2, &res)
				Expect(res).NotTo(BeZero())
				Expect(res.Rule.Archived).To(BeNil())
				Expect(res.Rule.Name).To(Equal("some name2"))
				Expect(res.Rule.ID).To(Equal(ID2))
				Expect(res.Rule.URN).To(Equal(urn2))
				data := new(models.RuleData)
				utils.MustMarshalUnmarshalJSON(res.Rule.Data, data)
				Expect(data.Schedule).To(Equal("every 30 min"))
				Expect(data.RegExp).To(Equal("exp2"))
				Expect(res.Rule.Created).To(BeNumerically("~", now-5, now+5))
				Expect(res.Rule.Updated).To(BeNumerically("~", now-5, now+5))
				Expect(res.Rule.Archived).To(BeNil())
			})
		})
		Context("update rule", func() {
			It("checks that update field changed", func() {
				var updateBefore int64
				By("get company", func() {
					res := new(rule.GetResponse)
					g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Rule).NotTo(BeZero())
					updateBefore = res.Rule.Updated
					Expect(updateBefore).To(BeNumerically(">", 0))
				})
				waitNextSecond()
				By("update fields", func() {
					data := []byte(`{"name":"new name"}`)
					g.PerformRuleUpdateRequest(http.StatusOK, ID1, data, nil)
				})
				By("getting updated record", func() {
					res := new(rule.GetResponse)
					g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Rule).NotTo(BeZero())
					Expect(res.Rule.Updated).To(BeNumerically(">", updateBefore))
					Expect(res.Rule.Updated).To(BeNumerically("<", updateBefore+5)) // not too far
				})
			})

			Context("name field", func() {
				It("checks if OK", func() {
					By("updating", func() {
						data := []byte(`{"name":"new name"}`)
						g.PerformRuleUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(rule.GetResponse)
						g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Rule).NotTo(BeZero())
						Expect(res.Rule.Name).To(Equal("new name"))
						Expect(res.Rule.ID).To(Equal(ID1))
						Expect(res.Rule.URN).To(Equal(urn1))
					})
				})

				It("checks if empty string", func() {
					By("updating", func() {
						data := []byte(`{"name":""}`)
						g.PerformRuleUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(rule.GetResponse)
						g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Rule).NotTo(BeZero())
						Expect(res.Rule.Name).To(BeEmpty())
					})
				})
			})
			Context("data field", func() {
				It("checks that can't set a JSON non-object", func() {
					By("updating", func() {
						data := []byte(`{"data":["1","2"]}`)
						g.PerformRuleUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						var res rule.GetResponse
						g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Rule).NotTo(BeZero())
						Expect(res.Rule.Name).To(Equal("some name"))
						Expect(res.Rule.Data).To(MatchJSON(`{"schedule":"every 60 min","regexp":"exp1","metrics":{"terms":null}}`))
					})
				})

				It("checks updating to object", func() {
					By("updating", func() {
						data := []byte(`{"data":{"schedule":"1"}}`)
						g.PerformRuleUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						var res rule.GetResponse
						g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Rule).NotTo(BeZero())
						Expect(res.Rule.Name).To(Equal("some name"))
						Expect(res.Rule.CompanyID.String()).To(Equal(compID1.String()))
						Expect(res.Rule.Data).To(MatchJSON(`{"schedule":"1","regexp":"","metrics":{"terms":null}}`))
					})
				})

				It("checks that can't set to null", func() {
					By("updating", func() {
						data := []byte(`{"data":null}`)
						g.PerformRuleUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						var res rule.GetResponse
						g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Rule).NotTo(BeZero())
						Expect(res.Rule.Name).To(Equal("some name"))
						Expect(res.Rule.Data).To(MatchJSON(`{"schedule":"every 60 min","regexp":"exp1","metrics":{"terms":null}}`))
					})
				})
			})

		})
		Context("archive, restore, remove rule", func() {
			It("checks if not found", func() {
				g.PerformRuleArchiveRequest(http.StatusOK, noID, nil)
				g.PerformRuleRestoreRequest(http.StatusOK, noID, nil)
				g.PerformRuleRemoveRequest(http.StatusOK, noID, nil)
			})

			It("checks archive and restore", func() {
				By("archiving twice", func() {
					g.PerformRuleArchiveRequest(http.StatusOK, ID1, nil)
					g.PerformRuleArchiveRequest(http.StatusOK, ID1, nil)
				})
				By("getting rule", func() {
					res := new(rule.GetResponse)
					nowUnix := models.TimeNow().Unix()
					g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Rule.Archived).NotTo(BeNil())
					Expect(*res.Rule.Archived).To(BeNumerically("~", nowUnix, 2))
				})

				By("restoring twice", func() {
					g.PerformRuleRestoreRequest(http.StatusOK, ID1, nil)
					g.PerformRuleRestoreRequest(http.StatusOK, ID1, nil)
				})
				By("getting", func() {
					var res rule.GetResponse
					g.PerformRuleGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Rule.Archived).To(BeNil())
				})
			})

			It("checks remove", func() {
				g.PerformRuleRemoveRequest(http.StatusOK, ID1, nil)
				g.PerformRuleArchiveRequest(http.StatusOK, ID1, nil)
				g.PerformRuleRemoveRequest(http.StatusOK, ID1, nil)
				By("trying to get deleted rule", func() {
					var res response.Default
					g.PerformRuleGetRequest(http.StatusNotFound, ID1, &res)
				})
			})
		})
	})
	Describe("signals API", func() {
		var ID2, ID1, compID1, compID2, noID strfmt.UUID
		var kind1, kind2, caption1, caption2, msg1, msg2 string

		BeforeEach(func() {
			ID1 = strfmt.UUID(uuid.NewV4().String())
			ID2 = strfmt.UUID(uuid.NewV4().String())
			compID1 = strfmt.UUID(uuid.NewV4().String())
			compID2 = strfmt.UUID(uuid.NewV4().String())
			noID = strfmt.UUID(uuid.NewV4().String())
			kind1 = "kind1"
			kind2 = "kind2"
			caption1 = "caption1"
			caption2 = "caption2"
			msg1 = "msg1"
			msg2 = "msg2"

			By("creating signal 1", func() {
				req := fmt.Sprintf(`{"sgid":%q, "compid": %q, "kind":%q, "caption": %q, "msg":%q, "data": {"schedule": "every 60 min", "regexp":"exp1"}}`, ID1, compID1, kind1, caption1, msg1)
				res := new(signal.AddResponse)
				g.PerformSignalCreateRequest(http.StatusCreated, []byte(req), &res)
				Expect(res.ID).To(Equal(ID1))

			})
			By("creating signal 2", func() {
				req := fmt.Sprintf(`{"sgid":%q, "compid": %q, "kind":%q, "caption": %q, "msg": %q, "data": {"schedule": "every 30 min", "regexp":"exp2"}}`, ID2, compID2, kind2, caption2, msg2)
				res := new(signal.AddResponse)
				g.PerformSignalCreateRequest(http.StatusCreated, []byte(req), &res)
				Expect(res.ID).To(Equal(ID2))
			})
		})

		It("checks listing all signals", func() {
			var res signal.ListResponse
			g.PerformSignalListRequest(http.StatusOK, nil, &res)
			Expect(res.Items).To(HaveLen(2))
			Expect(res.Items.IDs()).To(ConsistOf([]strfmt.UUID{ID1, ID2}))
		})

		It("checks listing signals with given kind", func() {
			var res signal.ListResponse
			data := url.Values{"kinds": {kind1}}
			g.PerformSignalListRequest(http.StatusOK, data, &res)
			Expect(res.Items).To(HaveLen(1))
			Expect(res.Items[0].Kind).To(Equal(kind1))
		})

		It("checks listing signals by search string", func() {
			res := new(signal.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("searchStr", caption1)
			urlVals.Add("search", "caption")
			g.PerformSignalListRequest(http.StatusOK, urlVals, &res)
			Expect(res.Items).To(HaveLen(1))
			Expect(res.Items[0].Caption).To(Equal(caption1))
		})

		It("checks listing signals by search string", func() {
			res := new(signal.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("searchStr", kind2)
			urlVals.Add("search", "kind")
			g.PerformSignalListRequest(http.StatusOK, urlVals, &res)
			Expect(res.Items).To(HaveLen(1))
			Expect(res.Items[0].Caption).To(Equal(caption2))
			Expect(res.Items[0].Kind).To(Equal(kind2))
		})

		It("checks listing signals from to all", func() {
			now := time.Now().Unix()
			res := new(signal.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("updatedfrom", ut.ToString(now-5))
			urlVals.Add("updatedto", ut.ToString(now+5))
			g.PerformSignalListPostRequest(http.StatusOK, urlVals, nil, &res)
			Expect(res.Items).To(HaveLen(2))
		})

		It("checks listing signals from to should be empty", func() {
			now := time.Now().Unix()
			res := new(signal.ListResponse)
			urlVals := url.Values{}
			urlVals.Add("updatedfrom", ut.ToString(now-25))
			urlVals.Add("updatedto", ut.ToString(now-5))
			g.PerformSignalListPostRequest(http.StatusOK, urlVals, nil, &res)
			Expect(res.Items).To(HaveLen(0))
		})

		Context("get signal", func() {
			It("checks getting signal if exists", func() {
				now := time.Now().Unix()
				res := new(signal.GetResponse)
				g.PerformSignalGetRequest(http.StatusOK, ID2, &res)
				Expect(res).NotTo(BeZero())
				Expect(res.Signal.Archived).To(BeNil())
				Expect(res.Signal.Kind).To(Equal(kind2))
				Expect(res.Signal.ID).To(Equal(ID2))
				Expect(res.Signal.Caption).To(Equal(caption2))
				Expect(res.Signal.Msg).To(Equal(msg2))
				Expect(res.Signal.Created).To(BeNumerically("~", now-5, now+5))
				Expect(res.Signal.Updated).To(BeNumerically("~", now-5, now+5))
				Expect(res.Signal.Archived).To(BeNil())
			})
		})

		Context("update signal", func() {
			It("checks that update field changed", func() {
				var updateBefore int64
				By("get signal", func() {
					res := new(signal.GetResponse)
					g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Signal).NotTo(BeZero())
					updateBefore = res.Signal.Updated
					Expect(updateBefore).To(BeNumerically(">", 0))
				})
				waitNextSecond()
				By("update fields", func() {
					data := []byte(`{"caption":"new name"}`)
					g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
				})
				By("getting updated record", func() {
					res := new(signal.GetResponse)
					g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Signal).NotTo(BeZero())
					Expect(res.Signal.Updated).To(BeNumerically(">", updateBefore))
					Expect(res.Signal.Updated).To(BeNumerically("<", updateBefore+5)) // not too far
				})
			})

			Context("caption field", func() {
				It("checks if OK", func() {
					By("updating", func() {
						data := []byte(`{"caption":"new name"}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Caption).To(Equal("new name"))
						Expect(res.Signal.ID).To(Equal(ID1))
					})
				})

				It("checks if empty string", func() {
					By("updating", func() {
						data := []byte(`{"caption":""}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Caption).To(BeEmpty())
					})
				})

				It("checks if bad value", func() {
					By("updating", func() {
						data := []byte(`{"caption":123456}`)
						g.PerformSignalUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Caption).To(Equal(caption1))
					})
				})
			})

			Context("msg field", func() {
				It("checks if OK", func() {
					By("updating", func() {
						data := []byte(`{"msg":"new msg"}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Msg).To(Equal("new msg"))
						Expect(res.Signal.ID).To(Equal(ID1))
					})
				})

				It("checks if empty string", func() {
					By("updating", func() {
						data := []byte(`{"msg":""}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Msg).To(BeEmpty())
					})
				})

				It("checks if bad value", func() {
					By("updating", func() {
						data := []byte(`{"msg":123456}`)
						g.PerformSignalUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Msg).To(Equal(msg1))
					})
				})
			})

			Context("comment field", func() {
				It("checks if OK", func() {
					By("updating", func() {
						data := []byte(`{"comm":"new comm"}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Comment).To(Equal("new comm"))
						Expect(res.Signal.ID).To(Equal(ID1))
					})
				})

				It("checks if empty string", func() {
					By("updating", func() {
						data := []byte(`{"comm":""}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Comment).To(BeEmpty())
					})
				})

				It("checks if bad value", func() {
					By("updating", func() {
						data := []byte(`{"comm":123456}`)
						g.PerformSignalUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						res := new(signal.GetResponse)
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Comment).To(BeEmpty())
					})
				})
			})

			Context("data field", func() {
				It("checks that can't set a JSON non-object", func() {
					By("updating", func() {
						data := []byte(`{"data":["1","2"]}`)
						g.PerformSignalUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						var res signal.GetResponse
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Caption).To(Equal(caption1))
						Expect(res.Signal.Data).To(MatchJSON(`{"schedule": "every 60 min", "regexp":"exp1"}`))
					})
				})

				It("checks updating to object", func() {
					By("updating", func() {
						data := []byte(`{"data":{"schedule":"1"}}`)
						g.PerformSignalUpdateRequest(http.StatusOK, ID1, data, nil)
					})
					By("selecting", func() {
						var res signal.GetResponse
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Caption).To(Equal(caption1))
						Expect(res.Signal.CompanyID.String()).To(Equal(compID1.String()))
						Expect(res.Signal.Data).To(MatchJSON(`{"schedule":"1"}`))
					})
				})

				It("checks that can't set to null", func() {
					By("updating", func() {
						data := []byte(`{"data":null}`)
						g.PerformSignalUpdateRequest(http.StatusUnprocessableEntity, ID1, data, nil)
					})
					By("selecting", func() {
						var res signal.GetResponse
						g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
						Expect(res.Signal).NotTo(BeZero())
						Expect(res.Signal.Caption).To(Equal(caption1))
						Expect(res.Signal.Data).To(MatchJSON(`{"schedule":"every 60 min","regexp":"exp1"}`))
					})
				})
			})

		})
		Context("archive, restore, remove signal", func() {
			It("checks if not found", func() {
				g.PerformSignalArchiveRequest(http.StatusOK, noID, nil)
				g.PerformSignalRestoreRequest(http.StatusOK, noID, nil)
				g.PerformSignalRemoveRequest(http.StatusOK, noID, nil)
			})

			It("checks archive and restore", func() {
				By("archiving twice", func() {
					g.PerformSignalArchiveRequest(http.StatusOK, ID1, nil)
					g.PerformSignalArchiveRequest(http.StatusOK, ID1, nil)
				})
				By("getting signal", func() {
					res := new(signal.GetResponse)
					nowUnix := models.TimeNow().Unix()
					g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Signal.Archived).NotTo(BeNil())
					Expect(*res.Signal.Archived).To(BeNumerically("~", nowUnix, 2))
				})

				By("restoring twice", func() {
					g.PerformSignalRestoreRequest(http.StatusOK, ID1, nil)
					g.PerformSignalRestoreRequest(http.StatusOK, ID1, nil)
				})
				By("getting", func() {
					var res signal.GetResponse
					g.PerformSignalGetRequest(http.StatusOK, ID1, &res)
					Expect(res.Signal.Archived).To(BeNil())
				})
			})

			It("checks remove", func() {
				g.PerformSignalRemoveRequest(http.StatusOK, ID1, nil)
				g.PerformSignalArchiveRequest(http.StatusOK, ID1, nil)
				g.PerformSignalRemoveRequest(http.StatusOK, ID1, nil)
				By("trying to get deleted content", func() {
					var res response.Default
					g.PerformSignalGetRequest(http.StatusNotFound, ID1, &res)
				})
			})
		})

		// todo tests for metrics API
	})
})
