package main

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"monitoring-for-wis/api"
	"monitoring-for-wis/api/rule"
	"monitoring-for-wis/api/signal"
	"monitoring-for-wis/client/dbcore"
	"monitoring-for-wis/client/geonetwork"
	"monitoring-for-wis/config"
	"monitoring-for-wis/keeper"
	"monitoring-for-wis/monitoring"
	"monitoring-for-wis/resolver"
	"monitoring-for-wis/service"
)

// RegisterHTTPAPIHandlers registers HTTP API handlers
func RegisterHTTPAPIHandlers(router *echo.Echo) {
	router.Use(middleware.Recover())
	router.Use(middleware.CORS())
	router.Use(middleware.Gzip())
	router.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
		fmt.Printf("@@@@ URI: %s\n", c.Request().RequestURI)
		fmt.Printf("@@@@ REQUEST: %s\n", reqBody)
		fmt.Printf("@@@@ RESPONSE: %s\n", resBody)
	}))

	v1 := router.Group("/api/v1")
	rules := v1.Group("/rules")
	rules.POST("/new", rule.Add)
	rules.GET("/list", rule.List)
	rules.POST("/list", rule.List)
	rules.GET("/:id", rule.Get)
	rules.POST("/:id/update", rule.Update)
	rules.POST("/archive", rule.Archive)
	rules.POST("/restore", rule.Restore)
	rules.POST("/rem", rule.Remove)

	signals := v1.Group("/signals")
	signals.POST("/new", signal.Add)
	signals.GET("/list", signal.List)
	signals.POST("/list", signal.List)
	signals.GET("/:id", signal.Get)
	signals.POST("/:id/update", signal.Update)
	signals.POST("/archive", signal.Archive)
	signals.POST("/restore", signal.Restore)
	signals.POST("/rem", signal.Remove)

	system := v1.Group("/system")
	system.GET("", api.DCSystem)
}

func initLogLevel(conf *viper.Viper) error {
	logLevel, err := logrus.ParseLevel(conf.GetString(config.LogLevel))
	if err != nil {
		return err
	}
	logrus.SetLevel(logLevel)
	return nil
}

func main() {
	conf, err := config.Parse()
	if err != nil {
		logrus.Fatalln(err)
	}

	if err = initLogLevel(conf); err != nil {
		logrus.Fatalln(err)
	}

	if err = service.New(conf); err != nil {
		logrus.Fatalln(err)
	}

	s := service.Get()

	var serviceResolver resolver.Resolver
	serviceResolver = resolver.NewConfigResolver(viper.GetViper())

	gnClient := geonetwork.New(s.HTTPClient, serviceResolver)
	dcClient := dbcore.New(s.HTTPClient, serviceResolver)
	s.SetGNClient(gnClient)
	s.SetDCClient(dcClient)
	configuration := config.Clone()
	var storage keeper.Keeper
	switch configuration.Storage.Type {
	case config.StorageTypeObjectsService:
		host := configuration.Services[resolver.ServiceObjectStorage].Host
		path := configuration.Services[resolver.ServiceObjectStorage].Path
		if len(host) == 0 {
			host = configuration.Storage.Host
		}
		storage = keeper.NewObjectsService(keeper.ObjectsServiceParams{
			Host:    host + path,
			Timeout: configuration.HTTPClient.TimeoutDuration(),
		})
	case config.StorageTypeMemory:
		storage = keeper.NewMemory()
	default:
		logrus.Fatalln("storage type is invalid")
	}
	if err != nil {
		logrus.Fatalln(err)
	}
	s.SetKeeper(storage)

	mon, err := monitoring.New(dcClient, gnClient, storage)
	if err != nil {
		logrus.Fatalln(err)
	}
	s.SetMonitoring(mon)

	RegisterHTTPAPIHandlers(s.HTTPServer)
	if err = s.HTTPServer.Start(fmt.Sprintf(":%d", config.Clone().Port)); err != nil {
		s.Logger.Fatalf("HTTP server error: %v", err)
	}
}
