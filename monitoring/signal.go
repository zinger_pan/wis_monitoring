package monitoring

import (
	"context"

	"github.com/go-openapi/strfmt"
	"monitoring-for-wis/keeper/models"
)

// AddSignal add signal
func (m *Monitoring) AddSignal(ctx context.Context, content models.Signal) (strfmt.UUID, error) {
	obj := content.ToObjectsServiceObject()
	uuids, err := m.keeper.Create(ctx, []models.ObjectsServiceObject{obj})
	return uuids[0], err
}

// ListSignals get list signals via given parameters
func (m *Monitoring) ListSignals(ctx context.Context, pp models.ListSignalParams) ([]models.Signal, int, error) {
	objList := pp.ToObjectsServiceListParams()
	objects, total, err := m.keeper.List(ctx, objList)

	signals := make([]models.Signal, len(objects))
	for i, obj := range objects {
		signals[i] = obj.ToSignal()
	}
	return signals, total, err
}

// GetSignal get signal by ID
func (m *Monitoring) GetSignal(ctx context.Context, ID strfmt.UUID) (*models.Signal, error) {
	obj, err := m.keeper.Get(ctx, ID)
	if err != nil {
		return nil, err
	}

	if obj == nil {
		return nil, err
	}
	signal := obj.ToSignal()
	return &signal, nil
}

// UpdateSignal update signal by ID with fields
func (m *Monitoring) UpdateSignal(ctx context.Context, ID strfmt.UUID, fields map[string]interface{}) error {
	return m.keeper.Update(ctx, ID, fields)
}

// ArchiveSignal signal by ID
func (m *Monitoring) ArchiveSignal(ctx context.Context, ID strfmt.UUID) error {
	return m.keeper.Archive(ctx, ID)
}

// RestoreSignal signal by ID
func (m *Monitoring) RestoreSignal(ctx context.Context, ID strfmt.UUID) error {
	return m.keeper.Restore(ctx, ID)
}

// RemoveSignal signal by ID
func (m *Monitoring) RemoveSignal(ctx context.Context, ID strfmt.UUID) error {
	return m.keeper.Remove(ctx, ID)
}
