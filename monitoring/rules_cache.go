package monitoring

import (
	"sync"

	"monitoring-for-wis/keeper/models"
)

type rulesCache struct {
	sync.Mutex
	cache map[string]models.Rule
}

func newRulesCache() *rulesCache { return &rulesCache{cache: make(map[string]models.Rule)} }

// addRules adds or overwrites multiple rules
func (rc *rulesCache) addRules(rules []models.Rule) {
	rc.Lock()
	defer rc.Unlock()
	for _, rule := range rules {
		rc.cache[rule.URN] = rule
	}
}

// urns returns a slice contains all rules' URNs
func (rc *rulesCache) urns() (res []string) {
	rc.Lock()
	defer rc.Unlock()
	res = make([]string, 0, len(rc.cache))
	for urn := range rc.cache {
		res = append(res, urn)
	}
	return
}

// cloneRules returns a deep copy of rules map
func (rc *rulesCache) cloneRules() (res map[string]models.Rule) {
	rc.Lock()
	defer rc.Unlock()
	res = make(map[string]models.Rule)
	for URN, rule := range rc.cache {
		res[URN] = rule.Clone()
	}
	return
}

func (rc *rulesCache) isIntersects(rules []models.Rule) bool {
	urns := make([]string, len(rules))
	for i, rule := range rules {
		urns[i] = rule.URN
	}

	rc.Lock()
	defer rc.Unlock()
	for _, urn := range urns {
		if _, ok := rc.cache[urn]; ok {
			return true
		}
	}
	return false
}
