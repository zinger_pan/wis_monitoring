package monitoring

// Color represents a color with name and value
type Color struct {
	Name  string
	Value int
}

// DS colors (string)
var (
	ColorGreen  = Color{Name: "green", Value: 0x32CD32}
	ColorOrange = Color{Name: "orange", Value: 0xFFA500}
	ColorRed    = Color{Name: "red", Value: 0xFF0000}
	ColorGray   = Color{Name: "gray", Value: 0x666666}
	Colors      = []Color{ColorGreen, ColorOrange, ColorRed}
)

func GetColorByArrivalDeadlineStatement(dState ArrivalDeadlineStatement) Color {
	switch dState {
	case ArrivalExpected:
		return ColorGray
	case ArrivalInTime:
		return ColorGreen
	case Arrival1HourLate, Arrival3HoursLate:
		return ColorOrange
	default:
		return ColorRed
	}
}

// ColorValue returns color value by name
func ColorValue(name string) int {
	for _, color := range Colors {
		if color.Name == name {
			return color.Value
		}
	}
	return -1
}
