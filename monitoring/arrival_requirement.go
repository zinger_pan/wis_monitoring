package monitoring

import (
	//"pkg/mod/github.com/robfig/cron/v3"
	"time"

	"monitoring-for-wis/keeper/models"
)

// ArrivalDeadlineStatement is an abstraction over data arrival statement
type ArrivalDeadlineStatement int

// data arrival deadline constant values
const (
	ArrivalExpected           ArrivalDeadlineStatement = iota // should arrive (planned)
	ArrivalNotHappened                                        // not arrived
	ArrivalTooEarly                                           // arrived earlier than observation term
	ArrivalInTime                                             // arrived within the deadline
	Arrival1HourLate                                          // arrived within one hour after deadline
	Arrival3HoursLate                                         // arrived within three hours after deadline
	ArrivalMoreThan3HoursLate                                 // arrived later than three hours after deadline
)

// arrivalCheck checks the data arrival deadline. It should return the right arrival deadline statement
/*func (m *Monitoring) arrivalCheck(rule models.Rule, meteoTerm time.Time, data dbcore.JournalRecord) (ArrivalDeadlineStatement, error) {
	// todo implement consideration logic
	return ArrivalExpected, nil
}*/

// arrivalCheck checks the data arrival deadline. It should return the right arrival deadline statement
func (m *Monitoring) arrivalCheck(rule models.Rule, meteoTerm time.Time, data models.JournalRecord, mtExpired time.Duration,
	mtEarlyProtectedPeriod time.Duration) (ArrivalDeadlineStatement, Color, error) {

	fmtData, err := data.Fmt()
	color := ColorRed
	if err != nil {
		return ArrivalNotHappened, color, err
	}
	jrTime := time.Unix(data.TimeStamp, 0).UTC()
	if jrTime.Before(fmtData.Add(-mtEarlyProtectedPeriod)) {
		return ArrivalTooEarly, color, nil
	}
	color = ColorGreen
	if jrTime.Before(fmtData.Add(time.Second * -5)) {
		return ArrivalTooEarly, color, nil
	}
	if jrTime.Before(fmtData.Add(mtExpired)) {
		return ArrivalInTime, color, nil
	}
	color = ColorOrange
	if jrTime.Before(fmtData.Add(time.Hour * 1)) {
		return Arrival1HourLate, color, nil
	}
	if jrTime.Before(fmtData.Add(time.Hour * 3)) {
		return Arrival3HoursLate, color, nil
	}
	return ArrivalMoreThan3HoursLate, color, nil
}
