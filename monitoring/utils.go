package monitoring

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"monitoring-for-wis/client/dbcore"
	"monitoring-for-wis/keeper/models"
	"os"
	"strings"
	"unicode/utf8"
)

func testDataFromFile() []models.JournalRecord {
	jsonFile, err := os.Open("journalRecordResponse.json")
	if err != nil {
		fmt.Println(err)
	}
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	var r dbcore.ListJournalRecordResponse

	var base struct {
		Meta struct {
			Error string `json:"error"`
		} `json:"meta"`
		Response json.RawMessage `json:"response"`
	}

	if err := json.Unmarshal(byteValue, &base); err != nil {
		log.Fatalln(err)
	}

	if base.Meta.Error != "" {
		log.Fatalln(base.Meta.Error)
	}

	json.Unmarshal(base.Response, &r)

	return r.Records.Items
}

var removeNonUTF = func(r rune) rune {
	if r == utf8.RuneError {
		return -1
	}
	return r
}

// RemoveNonUTF8Strings removes strings that isn't UTF-8 encoded
func RemoveNonUTF8Strings(string string) string {
	return strings.Map(removeNonUTF, string)
}

// RemoveNonUTF8Bytes removes bytes that isn't UTF-8 encoded
func RemoveNonUTF8Bytes(data []byte) []byte {
	return bytes.Map(removeNonUTF, data)
}
