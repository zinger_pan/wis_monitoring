package monitoring_test

import (
	"context"
	"math/rand"
	"net/http"
	"strings"
	"testing"
	"time"

	"gorm.io/datatypes"

	"github.com/go-openapi/strfmt"
	dcclient "monitoring-for-wis/client/dbcore"
	gnclient "monitoring-for-wis/client/geonetwork"
	"monitoring-for-wis/config"
	"monitoring-for-wis/keeper"
	"monitoring-for-wis/keeper/models"
	dcmock "monitoring-for-wis/mock/dbcore"
	gnmock "monitoring-for-wis/mock/geonetwork"
	"monitoring-for-wis/monitoring"
	"monitoring-for-wis/resolver"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const httpClientTimeout = 300 * time.Millisecond // increase this value in slow environment

var (
	gnMock          *gnmock.MockServer
	dcMock          *dcmock.MockServer
	gnClient        *gnclient.GNClient
	dcClient        *dcclient.DCClient
	storage         keeper.Keeper
	m               *monitoring.Monitoring
	metricsToCreate []models.MetricMetrics
)

const (
	urn1  = "test:urn:1"
	urn2  = "test:urn:2"
	kind1 = "kind1"
	kind2 = "kind2"
)

func timeMustParse(t string) time.Time {
	res, err := time.Parse("2006-01-02T15:04:05", t)
	if err != nil {
		panic(err)
	}
	return res
}

var _ = Describe("register suite hooks", func() {
	BeforeSuite(func() {
		logrus.SetLevel(logrus.DebugLevel)
	})

	var (
		ID1 = strfmt.UUID(uuid.NewV4().String())
		ID2 = strfmt.UUID(uuid.NewV4().String())
	)
	BeforeEach(func() {
		By("creating mocks", func() {
			gnMock = gnmock.New()
			dcMock = dcmock.New()
			gnMock.SetBasicAuth("admin", "admin")
			dcMock.SetBasicAuth("admin", "admin")
		})

		By("initializing config", func() {
			viper.SetConfigType("yml")
			viper.Set("httpClient.timeout", 300)
			viper.AutomaticEnv()
			replacer := strings.NewReplacer(".", "_")
			viper.SetEnvKeyReplacer(replacer)
			Expect(config.ViperLoadIntoCurrent()).To(Succeed())
		})

		By("presetting configuration", func() {
			Expect(config.SoftSet(func(c *config.Entire) {
				c.Services = map[string]config.Service{
					resolver.ServiceGeoNetwork: {Host: gnMock.Server.URL, Path: "/geonetwork"},
					resolver.ServiceDbCore:     {Host: dcMock.Server.URL, Path: "/api/v2"},
				}
				c.Auth = map[string]string{
					resolver.ServiceGeoNetwork: "admin:admin",
					resolver.ServiceDbCore:     "admin:admin",
				}
				c.Tickers.DbCoreMessagesPollInterval = config.TickersDbCoreNewMessages
			})).To(Succeed())
		})

		By("initializing keeper, proxies and logic layer", func() {
			storage = keeper.NewObjectsService(keeper.NewTestsObjectsServiceParams())
			Expect(storage).NotTo(BeNil())
			Expect(storage.DeleteAll(context.Background())).To(Succeed())

			serviceResolver := resolver.NewConfigResolver(viper.GetViper())
			httpClient := &http.Client{
				Transport:     nil,
				CheckRedirect: nil,
				Jar:           nil,
				Timeout:       httpClientTimeout,
			}
			gnClient = gnclient.New(httpClient, serviceResolver)
			dcClient = dcclient.New(httpClient, serviceResolver)
			var err error
			m, err = monitoring.New(dcClient, gnClient, storage)
			Expect(err).NotTo(HaveOccurred())
		})

		metricsToCreate = []models.MetricMetrics{
			{
				Terms: []models.MetricMetricsTermsItem{
					{
						MeteoTerm:   strfmt.DateTime(timeMustParse("2021-06-18T14:00:00")),
						Requirement: 0,
						Created:     strfmt.DateTime(timeMustParse("2021-06-18T14:33:00")),
						Updated:     strfmt.DateTime(timeMustParse("2021-06-18T14:33:00")),
					},
				},
			},
			{
				Terms: []models.MetricMetricsTermsItem{
					{
						MeteoTerm:   strfmt.DateTime(timeMustParse("2021-06-18T14:10:00")),
						Requirement: 1,
						Created:     strfmt.DateTime(timeMustParse("2021-06-18T14:34:00")),
						Updated:     strfmt.DateTime(timeMustParse("2021-06-18T14:34:00")),
					},
				},
			},
		}
	})

	It("checks that created monitoring instance is not nil", func() {
		Expect(gnClient).NotTo(BeNil())
		Expect(dcClient).NotTo(BeNil())
		Expect(storage).NotTo(BeNil())
		Expect(m).NotTo(BeNil())
	})

	It("checks adding rules", func() {
		var (
			ctx context.Context
			err error
		)
		By("creating rules", func() {
			ctx, err = m.AddRules(context.Background(), []models.Rule{
				{ID: ID1, URN: urn1, RegExp: ""},
				{ID: ID2, URN: urn2, RegExp: ""},
			})
			Expect(err).NotTo(HaveOccurred())
		})
		By("checking created rules in the storage", func() {
			rules, _, err := m.ListRules(context.Background(), models.ListRulesParams{})
			Expect(err).NotTo(HaveOccurred())
			Expect(rules).To(HaveLen(2))
			urns := make([]string, len(rules))
			for i, rule := range rules {
				urns[i] = rule.URN
			}
			Expect(urns).To(ConsistOf([]string{urn1, urn2}))
		})
		By("waiting for async ops finish", func() {
			Eventually(ctx.Done()).Should(Or(Receive(), BeClosed()))
			rules, _, err := m.ListRules(context.Background(), models.ListRulesParams{})
			Expect(err).NotTo(HaveOccurred())
			Expect(rules).To(HaveLen(2))
			for i, rule := range rules {
				Expect(rule.Schedule).NotTo(BeEmpty(), "i=%d", i)
			}
		})
	})

	It("checks adding rule if there is already rule with such URN", func() {
		var (
			ctx context.Context
			err error
		)
		By("adding rule", func() {
			ctx, err = m.AddRules(context.Background(), []models.Rule{{ID: ID1, URN: urn1, RegExp: ""}})
			Expect(err).NotTo(HaveOccurred())
			Eventually(ctx.Done()).Should(Or(Receive(), BeClosed()))
		})
		By("adding rule with same ID", func() {
			ctx, err = m.AddRules(context.Background(), []models.Rule{{ID: ID1, URN: urn1, RegExp: ""}})
			Expect(err).To(Equal(monitoring.ErrOneOfRuleURNsAlreadyExists))
			Expect(ctx.Done()).To(BeNil())
		})
		By("checking that there is only one rule in storage", func() {
			rules, _, err := m.ListRules(context.Background(), models.ListRulesParams{})
			Expect(err).NotTo(HaveOccurred())
			Expect(rules).To(HaveLen(1))
		})
	})

	It("checks update data field in rule", func() {
		var (
			ctx = context.Background()
			err error
		)
		By("adding rule", func() {
			_, err = m.AddRules(context.Background(), []models.Rule{{ID: ID1, URN: urn1, RegExp: ""}})
			Expect(err).NotTo(HaveOccurred())
		})
		By("update rule with same ID", func() {
			rule, err := m.GetRuleByURN(ctx, urn1)
			Expect(err).NotTo(HaveOccurred())
			Expect(rule.Metrics.Terms).To(BeNil())

			rule.Metrics = metricsToCreate[0]
			field := map[string]interface{}{
				models.ObjectFieldData: rule.ToObjectData(),
			}
			err = m.UpdateRule(ctx, ID1, field)
			Expect(err).NotTo(HaveOccurred())
		})
		By("getting updated rule", func() {
			rule, err := m.GetRuleByURN(ctx, urn1)
			Expect(err).NotTo(HaveOccurred())
			Expect(rule.Metrics).ToNot(BeNil())
			Expect(rule.Metrics.Terms).To(Equal(metricsToCreate[0].Terms))

		})
	})

	It("checks get rule", func() {

		By("creating rules", func() {
			_, err := m.AddRules(context.Background(), []models.Rule{
				{ID: ID1, URN: urn1, Name: "test1", RegExp: ""},
				{ID: ID2, URN: urn2, Name: "test2", RegExp: ""},
			})
			Expect(err).NotTo(HaveOccurred())
		})

		rule, err := m.GetRuleByID(context.Background(), ID1)
		Expect(err).NotTo(HaveOccurred())
		Expect(rule.ID).To(Equal(ID1))
		Expect(rule.ID).NotTo(BeEmpty())
		Expect(rule.Name).To(Equal("test1"))
		Expect(rule.Name).NotTo(BeEmpty())
	})
	It("checks restore, archive,remove content", func() {
		By("creating rules", func() {
			_, err := m.AddRules(context.Background(), []models.Rule{
				{ID: ID1, URN: urn1, Name: "test1", RegExp: ""},
				{ID: ID2, URN: urn2, Name: "test2", RegExp: ""},
			})
			Expect(err).NotTo(HaveOccurred())
		})
		By("archived", func() {
			err := m.ArchiveRule(context.Background(), ID1)
			Expect(err).NotTo(HaveOccurred())

			By("getting", func() {
				object, _ := m.GetRuleByID(context.Background(), ID1)
				Expect(&object.Archived).NotTo(BeNil())
			})
		})

		By("restore", func() {
			err := m.RestoreRule(context.Background(), ID1)
			Expect(err).NotTo(HaveOccurred())

			By("getting", func() {
				object, _ := m.GetRuleByID(context.Background(), ID1)
				Expect(object.Archived).To(BeNil())
			})
		})

		By("remove", func() {
			By("archived", func() {
				err := m.ArchiveRule(context.Background(), ID1)
				Expect(err).NotTo(HaveOccurred())
			})
			By("remove", func() {
				err := m.RemoveRule(context.Background(), ID1)
				Expect(err).NotTo(HaveOccurred())

				By("getting", func() {
					object, _ := m.GetRuleByID(context.Background(), ID1)
					Expect(object).To(BeNil())
				})
			})
		})
	})

	Context("signals", func() {
		It("checks adding", func() {
			By("creating signals", func() {
				id, err := m.AddSignal(context.Background(), models.Signal{
					ID: ID1, Kind: kind1, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID1))

				id, err = m.AddSignal(context.Background(), models.Signal{
					ID: ID2, Kind: kind2, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID2))
			})
			By("checking created signals in the storage", func() {
				signlas, _, err := m.ListSignals(context.Background(), models.ListSignalParams{})
				Expect(err).NotTo(HaveOccurred())
				Expect(signlas).To(HaveLen(2))
				kinds := make([]string, len(signlas))
				for i, signal := range signlas {
					kinds[i] = signal.Kind
				}
				Expect(kinds).To(ConsistOf([]string{kind1, kind2}))
			})
		})

		It("checks update data field in signal", func() {
			By("adding rule", func() {
				id, err := m.AddSignal(context.Background(), models.Signal{ID: ID1, Kind: kind1, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID1))
			})
			By("update signal with same ID", func() {
				signal, err := m.GetSignal(context.Background(), ID1)
				Expect(err).NotTo(HaveOccurred())
				Expect(signal.Kind).To(Equal(kind1))

				comment := "tratata"
				field := map[string]interface{}{
					models.ObjectFieldProp3: comment,
				}
				err = m.UpdateSignal(context.Background(), ID1, field)
				Expect(err).NotTo(HaveOccurred())
			})
			By("getting updated signal", func() {
				signal, err := m.GetSignal(context.Background(), ID1)
				Expect(err).NotTo(HaveOccurred())
				Expect(signal.Comment).ToNot(BeEmpty())
				Expect(signal.Comment).To(Equal("tratata"))

			})
		})

		It("checks get signal", func() {
			By("creating signal", func() {
				id, err := m.AddSignal(context.Background(), models.Signal{
					ID: ID1, Kind: kind1, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID1))

				id, err = m.AddSignal(context.Background(), models.Signal{
					ID: ID2, Kind: kind2, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID2))
			})

			signal, err := m.GetSignal(context.Background(), ID1)
			Expect(err).NotTo(HaveOccurred())
			Expect(signal.ID).To(Equal(ID1))
			Expect(signal.ID).NotTo(BeEmpty())
			Expect(signal.Kind).To(Equal(kind1))
			Expect(signal.Kind).NotTo(BeEmpty())
		})
		It("checks restore, archive,remove signal", func() {
			By("creating signal", func() {
				id, err := m.AddSignal(context.Background(), models.Signal{
					ID: ID1, Kind: kind1, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID1))

				id, err = m.AddSignal(context.Background(), models.Signal{
					ID: ID2, Kind: kind2, Data: datatypes.JSON(`{}`)})
				Expect(err).NotTo(HaveOccurred())
				Expect(id).To(Equal(ID2))
			})
			By("archived", func() {
				err := m.ArchiveSignal(context.Background(), ID1)
				Expect(err).NotTo(HaveOccurred())

				By("getting", func() {
					signal, _ := m.GetSignal(context.Background(), ID1)
					Expect(&signal.Archived).NotTo(BeNil())
				})
			})

			By("restore", func() {
				err := m.RestoreSignal(context.Background(), ID1)
				Expect(err).NotTo(HaveOccurred())

				By("getting", func() {
					signal, _ := m.GetSignal(context.Background(), ID1)
					Expect(signal.Archived).To(BeNil())
				})
			})

			By("remove", func() {
				By("archived", func() {
					err := m.ArchiveSignal(context.Background(), ID1)
					Expect(err).NotTo(HaveOccurred())
				})
				By("remove", func() {
					err := m.RemoveSignal(context.Background(), ID1)
					Expect(err).NotTo(HaveOccurred())

					By("getting", func() {
						signal, _ := m.GetSignal(context.Background(), ID1)
						Expect(signal).To(BeNil())
					})
				})
			})
		})
	})

	Context("start/stop", func() {
		//It("checks if no rules", func() { m.Stop() })
		// todo more tests

	})

})

func TestAll(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	RegisterFailHandler(Fail)
	RunSpecs(t, "Monitoring Suite")
}
