package monitoring

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"sync"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/go-openapi/strfmt"
	"github.com/senseyeio/duration"
	"github.com/sirupsen/logrus"
	"monitoring-for-wis/client/dbcore"
	"monitoring-for-wis/client/geonetwork"
	"monitoring-for-wis/config"
	"monitoring-for-wis/keeper"
	"monitoring-for-wis/keeper/models"
)

var (
	ErrNotFoundResourceMaintenanceInXML = errors.New("XML ResourceMaintenance was not found")
	ErrNotFoundUpdateDurationInXML      = errors.New("XML update duration was not found")
	ErrNotFoundTimeStartStringInXML     = errors.New("XML time start string was not found")
	ErrNotFoundTimeStartValueInXML      = errors.New("XML time start value not found in string")
	ErrOneOfRuleURNsAlreadyExists       = errors.New("one of rule URNs already exists")

	timeStartStringRegexp = regexp.MustCompile(`every \d+ \w+ starting at (?P<startHour>\d+) (?P<startHourTZ>\w+)`)
)

// Monitoring implements data flow monitoring
type Monitoring struct {
	dcClient *dbcore.DCClient
	gnClient *geonetwork.GNClient
	keeper   keeper.Keeper
	logger   logrus.FieldLogger

	cancelFunc   func()
	schedulerCtx context.Context // todo do we need this? may be used to wait for stop of the CRON scheduled task

	rules                *rulesCache
	lastDbCoreAccessTime time.Time // todo where to store this persistently and do we need it or not?
}

// New returns a pointer to a new instance of Monitoring
func New(dcClient *dbcore.DCClient, gnClient *geonetwork.GNClient, keeper keeper.Keeper) (*Monitoring, error) {
	ctx, cancelFunc := context.WithCancel(context.Background())
	m := &Monitoring{
		dcClient: dcClient,
		gnClient: gnClient,
		keeper:   keeper,
		logger:   logrus.StandardLogger().WithField("subsystem", "monitoring"),

		cancelFunc: cancelFunc,

		rules: newRulesCache(),
	}

	// preload rules into cache
	rules, _, err := m.ListRules(ctx, models.ListRulesParams{})
	if err != nil {
		return nil, err
	}

	m.rules.addRules(rules)

	go func() {
		if err := m.Start(ctx); err != nil {
			m.logger.Errorf("monitoring New() -> gofunc -> m.Start: %v", err)
		}
	}()
	return m, nil
}

// WaitForScheduledTasks to complete
func (m *Monitoring) WaitForScheduledTasks() {
	if m.schedulerCtx == nil {
		return
	}
	<-m.schedulerCtx.Done()
}

// Stop calls the cancelFunc if it was set
func (m *Monitoring) Stop() {
	if m.cancelFunc == nil {
		return
	}
	m.cancelFunc()
	return
}

// obtainRecordFromGeoNetwork and attempt to parse it to update our monitoring rule with Schedule field
func (m *Monitoring) obtainRecordFromGeoNetwork(ctx context.Context, intoRule *models.Rule) error {
	b, err := m.gnClient.GNGetRecord(ctx, geonetwork.GNGetRecordParams{ID: intoRule.URN})
	if err != nil {
		return err
	}

	xmlRootNode, err := xmlquery.Parse(bytes.NewReader(b))
	if err != nil {
		return err
	}

	xmlResourceMaintenanceNode := xmlquery.FindOne(xmlRootNode, "//gmd:resourceMaintenance")
	if xmlResourceMaintenanceNode == nil {
		return ErrNotFoundResourceMaintenanceInXML
	}

	xmlNode := xmlquery.FindOne(xmlResourceMaintenanceNode,
		"//gmd:MD_MaintenanceInformation/gmd:userDefinedMaintenanceFrequency/gts:TM_PeriodDuration")
	if xmlNode == nil {
		return ErrNotFoundUpdateDurationInXML
	}

	durationString := xmlNode.InnerText()
	d, err := duration.ParseISO8601(durationString)
	if err != nil {
		return err
	}
	m.logger.Debugln(">>>", d)

	if xmlNode = xmlquery.FindOne(xmlResourceMaintenanceNode,
		"//gmd:updateScopeDescription/gmd:MD_ScopeDescription/gmd:dataset/gco:CharacterString"); xmlNode == nil {
		return ErrNotFoundTimeStartStringInXML
	}
	m.logger.Debugln(">>>", xmlNode.InnerText())

	timeStartString := xmlNode.InnerText()
	if !timeStartStringRegexp.MatchString(timeStartString) {
		return ErrNotFoundTimeStartValueInXML
	}
	match := timeStartStringRegexp.FindStringSubmatch(timeStartString)
	var (
		startHour   int
		startHourTZ string
	)
	for i, name := range timeStartStringRegexp.SubexpNames() {
		if i == 0 || name == "" || match[i] == "" {
			continue
		}
		switch name {
		case "startHour":
			if startHour, err = strconv.Atoi(match[i]); err != nil {
				return err
			}
		case "startHourTZ":
			startHourTZ = match[i]
		}
	}

	loc, err := time.LoadLocation(startHourTZ)
	if err != nil {
		return err
	}
	timeStart := d.Shift(m.date(0).Add(-time.Duration(startHour) * time.Hour)).In(loc).UTC()
	m.logger.Debugln(">>TimeStart:", timeStart)
	intoRule.Schedule = fmt.Sprintf("%s/%s", timeStart.Format(time.RFC3339), d)
	return nil
}

// AddRules to monitoring. Returns a context to be able to wait for
// asynchronous operations completion. This may be useful for tests.
func (m *Monitoring) AddRules(ctx context.Context, rules []models.Rule) (context.Context, error) {
	if m.rules.isIntersects(rules) {
		return ctx, ErrOneOfRuleURNsAlreadyExists
	}

	m.rules.addRules(rules)

	var wg sync.WaitGroup
	wg.Add(len(rules))

	if err := m.createRules(ctx, rules); err != nil {
		return ctx, err
	}

	ctx, cancelFunc := context.WithCancel(ctx)
	go func() { // waiter goroutine, useful for tests
		wg.Wait()
		cancelFunc()
	}()

	for i := range rules { // asynchronously parsing and writing schedule
		go func(i int) { // updater goroutine
			defer wg.Done()
			if err := m.obtainRecordFromGeoNetwork(ctx, &rules[i]); err != nil {
				m.logger.Errorf("monitoring m.AddRules -> gofunc -> m.obtainRecordFromGeoNetwork: %v", err)
				return
			}

			fields := map[string]interface{}{
				models.ObjectFieldData: rules[i].ToObjectData(),
			}

			if err := m.keeper.Update(ctx, rules[i].ID, fields); err != nil {
				m.logger.Errorf("m.AddRules -> gofunc -> m.keeper.UpdateRule: %v", err)
				return
			}
			m.rules.addRules([]models.Rule{rules[i]})
		}(i)
	}

	return ctx, nil
}

func (m *Monitoring) now() time.Time { return time.Now().Truncate(time.Millisecond).UTC() }

func (m *Monitoring) date(days int) time.Time {
	return m.now().AddDate(0, 0, days).Truncate(24 * time.Hour)
}

func (m *Monitoring) loopViaRules(ctx context.Context) (err error) {
	if m.lastDbCoreAccessTime.IsZero() {
		m.lastDbCoreAccessTime = m.date(-1)
	}

	conf := config.Clone()
	rules := m.rules.cloneRules()
	mtExpired := config.Get().MTOffsets.MeteoTimeExpiredDuration()
	mtEarlyProtectedPeriod := config.Get().MTOffsets.MTEarlyProtectedPeriodDuration()
	var fromUUID strfmt.UUID
	for {
		journalRecords, err := m.dcClient.ListJournalRecords(ctx, dbcore.ListJournalRecordsParams{
			StartAt: m.lastDbCoreAccessTime,
			From:    fromUUID,
			URNs:    m.rules.urns(),
			Count:   conf.DefaultBatchSize,
		})
		if err != nil {
			return err
		}
		m.lastDbCoreAccessTime = m.now()
		if len(journalRecords) == 0 {
			break
		}
		journalURNs := make([]string, 0)
		metricUpdate := make(map[string][]models.MetricMetricsTermsItem)
		now := m.now()
		meteoTerms := []time.Time{now.Truncate(10 * time.Minute)} // todo do it in right way
		for _, jr := range journalRecords {
			// note: no need to check for rule existence here because we requested for the list of URNs
			rule := rules[jr.URN]
			journalURNs = append(journalURNs, jr.URN)
			jrTime := time.Unix(jr.TimeStamp, 0).UTC()

			for _, meteoTerm := range meteoTerms {
				arrivalRequirement, _, err := m.arrivalCheck(rule, meteoTerm, jr, mtExpired, mtEarlyProtectedPeriod)
				if err != nil {
					return err
				}

				metricUpdateValues := metricUpdate[jr.URN]
				metricUpdateValues = append(metricUpdateValues, models.MetricMetricsTermsItem{
					MeteoTerm:   strfmt.DateTime(meteoTerm), // todo do it in right way
					Requirement: int(arrivalRequirement),
					JRTime:      jrTime,
				})
				metricUpdate[jr.URN] = metricUpdateValues
			}
		}

		for urn, metricData := range metricUpdate {
			// if urn is empty, GetRuleByURN returned all rules
			if len(urn) == 0 {
				continue
			}
			storedRule, err := m.GetRuleByURN(ctx, urn)
			if err != nil {
				continue
			}

			for _, updateEntry := range metricData { // loop over data collected for update
				for _, meteoTerm := range meteoTerms { // loop over meteo terms for each update entry
					// is there an entry for this meteoTerm in the metrics?
					if termIndex := storedRule.Metrics.TermIndex(meteoTerm); termIndex >= 0 {
						// existing entry in Terms array
						updateEntry.Updated = strfmt.DateTime(updateEntry.JRTime)
						storedRule.Metrics.Terms[termIndex] = updateEntry
					} else { // new entry in Terms array
						updateEntry.Created = strfmt.DateTime(updateEntry.JRTime)
						updateEntry.Updated = strfmt.DateTime(updateEntry.JRTime)
						storedRule.Metrics.Terms = append(storedRule.Metrics.Terms, updateEntry)
					}
				}
			}

			fields := map[string]interface{}{
				models.ObjectFieldData: storedRule.ToObjectData(),
			}

			err = m.UpdateRule(ctx, storedRule.ID, fields)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Start the monitoring. Returns context to be able to wait for scheduler tasks to finish.
// Start should be called with context.WithCancel context.
func (m *Monitoring) Start(ctx context.Context) (err error) {
	confTickers := config.Clone().Tickers
	ticker := time.NewTicker(confTickers.DbCoreMessagesPollIntervalDuration())
	for {
		select {
		case <-ticker.C:
			if err = m.loopViaRules(ctx); err != nil {
				return err
			}
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

// createRules via parameters given
func (m *Monitoring) createRules(ctx context.Context, rules []models.Rule) error {
	objs := []models.ObjectsServiceObject{}
	for _, r := range rules {
		objs = append(objs, r.ToObjectsServiceObject())
	}

	if _, err := m.keeper.Create(ctx, objs); err != nil {
		return err
	}
	// todo check ids
	return nil
}

// ListRules via parameters given
func (m *Monitoring) ListRules(ctx context.Context, params models.ListRulesParams) ([]models.Rule, int, error) {
	objs, total, err := m.keeper.List(ctx, params.ToObjectsServiceListParams())
	rules := make([]models.Rule, len(objs))

	for i, obj := range objs {
		rules[i] = obj.ToRule()
	}
	return rules, total, err
}

// GetRuleByID rule
func (m *Monitoring) GetRuleByID(ctx context.Context, ID strfmt.UUID) (*models.Rule, error) {
	obj, err := m.keeper.Get(ctx, ID)

	if obj == nil {
		return nil, err
	}
	comp := obj.ToRule()
	return &comp, nil
}

// GetRuleByURN rule
func (m *Monitoring) GetRuleByURN(ctx context.Context, URN string) (*models.Rule, error) {
	params := models.ListRulesParams{
		URNs: []string{URN},
	}

	objs, _, err := m.keeper.List(ctx, params.ToObjectsServiceListParams())
	if err != nil {
		return nil, err
	}
	rule := objs[0].ToRule()
	return &rule, err
}

// UpdateRule rule
func (m *Monitoring) UpdateRule(ctx context.Context, ID strfmt.UUID, fields map[string]interface{}) error {

	if err := m.keeper.Update(ctx, ID, fields); err != nil {
		return err
	}
	return nil
}

// ArchiveRule rule
func (m *Monitoring) ArchiveRule(ctx context.Context, ID strfmt.UUID) error {
	return m.keeper.Archive(ctx, ID)
}

// RemoveRule rule
func (m *Monitoring) RemoveRule(ctx context.Context, ID strfmt.UUID) error {
	return m.keeper.Remove(ctx, ID)
}

// Loop this method for tests
func (m *Monitoring) Loop() error {
	return m.loopViaRules(context.Background())
}

// RestoreRule rule by ID
func (m *Monitoring) RestoreRule(ctx context.Context, ID strfmt.UUID) error {
	return m.keeper.Restore(ctx, ID)
}
