module monitoring-for-wis

go 1.16

require (
	github.com/AlekSi/pointer v1.1.0
	github.com/antchfx/xmlquery v1.3.6
	github.com/go-openapi/strfmt v0.20.1
	github.com/go-playground/validator/v10 v10.6.1
	github.com/google/uuid v1.1.2 // indirect
	github.com/labstack/echo/v4 v4.3.0
	github.com/mtfelian/deepcopy v1.0.0
	github.com/mtfelian/utils v1.0.7
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.13.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/satori/go.uuid v1.2.0
	github.com/senseyeio/duration v0.0.0-20180430131211-7c2a214ada46
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.7.1
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/datatypes v1.0.1
	gorm.io/driver/postgres v1.1.0 // indirect
	gorm.io/gorm v1.21.10
)
