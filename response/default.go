package response

import "fmt"

// error codes
const (
	OK = iota
	ErrBindingRequest
	ErrValidationRequest
	ErrFailedToResolveService
	ErrFailedToConstructURL
	ErrFailedToRequest
	ErrFailedToReadBody
	ErrFailedToParseBasicAuthInConfig
	ErrFailedToUnmarshalResponse
	ErrContentNotFound
)

// Default describes API error response
type Default struct {
	Code    int           `json:"code"`              // error code
	Message string        `json:"message,omitempty"` // default message with params substituted
	Params  []interface{} `json:"params,omitempty"`  // params for i18n localized messages
}

// Default makes Default to implement error
func (e Default) Error() string { return fmt.Sprintf("%d: %s", e.Code, e.Message) }
